//
//  SearchViewController.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 12/9/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

typealias OptionTuple = (String, String, Bool)

class SearchViewController: UIViewController {
  @IBOutlet weak var searchBar: UISearchBar!
  @IBOutlet weak var searchTableViewDelegate: SearchTableViewDelegate!
  @IBOutlet weak var tapGestureRecognizer: UITapGestureRecognizer!
  
  var pageSize = 50
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.addGestureRecognizer(tapGestureRecognizer)
    navigationItem.rightBarButtonItems = [UIBarButtonItem(image: UIImage(named: "icn_menu"), style: .Bordered, target: self, action: "showMenu")]

    for searchBarSubview in searchBar.subviews {
      for subviewSubview in searchBarSubview.subviews {
        if let textField = subviewSubview as? UITextField {
          textField.returnKeyType = .Next
        }
      }
    }
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if let cafeListViewController = segue.destinationViewController as? CafeListViewController {
      cafeListViewController.type = .Search
      if !IOS_8() {
        cafeListViewController.mainNavigationController = navigationController as? MainNavigationController
      }
      let filteredFeatures = self.searchTableViewDelegate.searchItems.filter({ (searchItem: SearchItem) -> Bool in
        searchItem.mode
      }).map({ (searchItem: SearchItem) -> Feature in
        searchItem.feature
      })
      cafeListViewController.searchTerm = searchBar.text
      cafeListViewController.searchFeatures = filteredFeatures
      SVProgressHUD.show()
      Cafe.getCafesWithTerm(searchBar.text, features: filteredFeatures, page: 1, perPage: pageSize, completion: { (cafes) -> () in
        if cafes != nil {
          cafeListViewController.tableViewDelegate.datasource = cafes!
          cafeListViewController.tableView.reloadData()
          if cafes!.count == 0 {
            cafeListViewController.tableView.hidden = true
            let label = UILabel(frame: CGRect(x: 30, y: 0, width: cafeListViewController.view.bounds.width - 60, height: cafeListViewController.view.bounds.height))
            label.text = "Sua busca não encontrou nenhum resultado."
            label.font = UIFont(name: "ClioBold-Bold", size: 18)
            label.textAlignment = .Center
            label.numberOfLines = 0
            cafeListViewController.view.addSubview(label)
          }
          SVProgressHUD.dismiss()
        } else {
          SVProgressHUD.showErrorWithStatus("Erro de conexão.")
        }
      })
    }
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
  }
  
  @IBAction func tapped(sender: UITapGestureRecognizer) {
    searchBar.resignFirstResponder()
  }
  
  @IBAction func searchTapped(sender: UIButton) {
    if IOS_8() {
      performSegueWithIdentifier("SegueSearchResults", sender: sender)
    } else {
      performSegueWithIdentifier("SegueSearchResults7", sender: sender)
    }
  }
  
  func showMenu() {
    if (navigationController as! MainNavigationController).menuViewController != nil {
      self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
    } else {
      self.navigationController?.performSegueWithIdentifier("SegueMenu", sender: nil)
    }
  }
}

extension SearchViewController: UISearchBarDelegate {
  func searchBarSearchButtonClicked(searchBar: UISearchBar) {
    searchBar.resignFirstResponder()
  }
}
