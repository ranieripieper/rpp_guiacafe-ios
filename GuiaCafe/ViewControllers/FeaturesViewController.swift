//
//  FeaturesViewController.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 12/11/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class FeaturesViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var featuresTableViewDelegate: FeaturesTableViewDelegate!
  
  var features: [Feature]?
  var mainNavigationController: MainNavigationController!
  var subview: UIView?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    featuresTableViewDelegate.features = features
    tableView.contentInset = UIEdgeInsets(top: 30, left: 0, bottom: 20, right: 0)
    if !IOS_8() {
      modalPresentationStyle = .CurrentContext
      modalPresentationStyle = .FormSheet
      subview = UIView(frame: mainNavigationController.view.frame)
      subview!.backgroundColor = UIColor.blackColor()
      subview!.alpha = 0.7
      mainNavigationController.view.addSubview(subview!)
    }
  }
  
  override func viewWillDisappear(animated: Bool) {
    super.viewWillDisappear(animated)
    subview?.removeFromSuperview()
    subview = nil
  }
  
  @IBAction func close(sender: UIButton) {
    dismissViewControllerAnimated(true, completion: nil)
  }
}
