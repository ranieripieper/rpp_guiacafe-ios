//
//  MenuViewController.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 12/9/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var menuTableViewDelegate: MenuTableViewDelegate!
  
  weak var mainNavigationController: MainNavigationController?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.contentInset = UIEdgeInsets(top: 64, left: 0, bottom: 0, right: 0)
    if !IOS_8() {
      modalPresentationStyle = .CurrentContext
      modalPresentationStyle = .FormSheet
    }
  }
  
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    menuTableViewDelegate.delegate = mainNavigationController
  }
}
