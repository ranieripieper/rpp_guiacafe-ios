//
//  MapViewController.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 12/1/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {
  @IBOutlet weak var mapView: MKMapView!
  
  var selectedCafe: Cafe?
  var bubbleAnnotation: BubbleAnnotation?
  var timer: NSTimer?
  var bubbleView: BubbleView?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configureNavigationBarButtons()
    let headerView = HeaderView(frame: CGRect(x: 0, y: 64, width: view.bounds.width, height: 30))
    headerView.configureWithState(State.SP)
    view.addSubview(headerView)
    refreshCafes()
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if let cafeDetailViewController = segue.destinationViewController as? CafeDetailViewController {
      if let cafe = sender as? Cafe {
        cafeDetailViewController.cafe = cafe
      }
      if !IOS_8() {
        cafeDetailViewController.mainNavigationController = navigationController as? MainNavigationController
      }
    }
    navigationItem.backBarButtonItem = UIBarButtonItem (title: "", style: .Bordered, target: nil, action: nil)
  }
  
  func configureNavigationBarButtons() {
    let menuButton = UIBarButtonItem(image: UIImage(named: "icn_menu"), style: .Bordered, target: self, action: "showMenu")
    let listButton = UIBarButtonItem(image: UIImage(named: "icn_list"), style: .Bordered, target: self, action: "showList")
    navigationItem.rightBarButtonItems = [menuButton, listButton]
  }
  
  func showMenu() {
    if (navigationController as! MainNavigationController).menuViewController != nil {
      navigationController?.dismissViewControllerAnimated(true, completion: nil)
    } else {
      navigationController?.performSegueWithIdentifier("SegueMenu", sender: nil)
    }
  }
  
  func showList() {
    navigationController?.viewControllers = [storyboard!.instantiateViewControllerWithIdentifier("CafeListViewController")]
  }
  
  func refreshCafes() {
    let locationManager = (navigationController as! MainNavigationController).locationManager
    let span = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
    let addToMap: [Cafe] -> () = { cafes in
      cafes.map() { cafe in
        self.mapView.addAnnotation(cafe)
      }
    }
    if locationManager.location != nil {
      Cafe.getAllCafesWithUserLocation(locationManager.location, page: 1, perPage: 50, completion: addToMap)
      mapView.setRegion(MKCoordinateRegion(center: locationManager.location.coordinate, span: span), animated: false)
    } else {
      Cafe.getAllCafesWithRatingAtPage(1, perPage: 50, completion: addToMap)
      mapView.setRegion(MKCoordinateRegion(center: mapView.region.center, span: span), animated: false)
    }
  }
}

extension MapViewController: MKMapViewDelegate {
  func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
    if let cafe = annotation as? Cafe {
      var annotationView = mapView.dequeueReusableAnnotationViewWithIdentifier("Cafe")
      if annotationView == nil {
        annotationView = MKAnnotationView (annotation: cafe, reuseIdentifier: "Cafe")
        annotationView.enabled = true
        annotationView.image = UIImage(named: cafe.state.info().2)
      } else {
        annotationView.annotation = cafe
      }
      if bubbleView != nil {
        mapView.bringSubviewToFront(bubbleView!)
      }
      return annotationView
    } else if let bubbleAnnotation = annotation as? BubbleAnnotation {
      bubbleView = mapView.dequeueReusableAnnotationViewWithIdentifier("BubbleAnnotation") as! BubbleView!
      if bubbleView == nil {
        bubbleView = BubbleView(annotation: bubbleAnnotation, reuseIdentifier: "BubbleAnnotation")
        bubbleView!.frame = CGRect(x: 0, y: 0, width: 300, height: 300)
        bubbleView!.enabled = true
      } else {
        bubbleView!.annotation = bubbleAnnotation
      }
      bubbleView!.configureWithCafe(bubbleAnnotation.cafe)
      bubbleView!.delegate = self
      bubbleView!.setRadiusWithPoint(mapView.convertCoordinate(selectedCafe!.coordinate, toPointToView: view), inView: view)
      mapView.bringSubviewToFront(bubbleView!)
      return bubbleView!
    }
    return nil
  }
  
  func mapView(mapView: MKMapView!, didSelectAnnotationView view: MKAnnotationView!) {
    if let cafe = view.annotation as? Cafe {
      if selectedCafe != cafe {
        if bubbleAnnotation != nil {
          mapView.removeAnnotation(bubbleAnnotation)
          bubbleAnnotation = nil
        }
        selectedCafe = cafe
        bubbleAnnotation = BubbleAnnotation(cafe: selectedCafe!, coordinate: cafe.coordinate)
        mapView.addAnnotation(bubbleAnnotation)
      }
    }
  }
  
  func mapView(mapView: MKMapView!, didDeselectAnnotationView view: MKAnnotationView!) {
    if let cafe = view.annotation as? Cafe {
      if bubbleAnnotation != nil && bubbleView != nil {
        bubbleView!.removeView()
      }
    }
  }
  
  func mapView(mapView: MKMapView!, regionDidChangeAnimated animated: Bool) {
    if bubbleAnnotation != nil {
      bubbleView!.setRadiusWithPoint(mapView.convertCoordinate(bubbleAnnotation!.cafe.coordinate, toPointToView: view), inView: view)
    }
    if bubbleView != nil && bubbleView!.superview != nil {
      bubbleView!.superview!.bringSubviewToFront(bubbleView!)
    }
  }
  
  func mapView(mapView: MKMapView!, didAddAnnotationViews views: [AnyObject]!) {
    if bubbleView != nil && bubbleView!.superview != nil {
      bubbleView!.superview!.bringSubviewToFront(bubbleView!)
    }
  }
}

extension MapViewController: CLLocationManagerDelegate {
  func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
    mapView.centerCoordinate = (locations.first as! CLLocation).coordinate
  }
}

extension MapViewController: BubbleViewDelegate {
  func didRemove() {
    if selectedCafe != nil {
      mapView.deselectAnnotation(selectedCafe!, animated: false)
    }
    if bubbleAnnotation != nil {
      mapView.removeAnnotation(bubbleAnnotation!)
    }
    selectedCafe = nil
    bubbleAnnotation = nil
  }
  
  func goToCafe() {
    if IOS_8() {
      performSegueWithIdentifier("SegueCafeDetail", sender: selectedCafe)
    } else {
      performSegueWithIdentifier("SegueCafeDetail7", sender: selectedCafe)
    }
  }
}
