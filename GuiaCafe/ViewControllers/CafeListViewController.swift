//
//  CafeListViewController.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 12/9/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

enum CafeListType {
  case All
  case Search
  case Top20
  case Newcomers
  case Favorites
}

class CafeListViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var tableViewDelegate: CafeListTableViewDelegate!
  
  var type: CafeListType = .All
  var sortView: SortView?
  var lastOffset = CGPointZero
  var alertView: AlertController?
  var currentPage = 1
  var loadingPage: Int?
  let pageSize = 50
  var searchTerm: String?
  var searchFeatures: [Feature]?
  var mainNavigationController: MainNavigationController?
  
  deinit {
    tableView.delegate = nil
    tableView.dataSource = nil
    tableViewDelegate = nil
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configure()
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    removeDefavoritedCafes()
    updateNewlyRatedCafe()
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if let cafeDetailViewController = segue.destinationViewController as? CafeDetailViewController {
      if let cafe = sender as? Cafe {
        cafeDetailViewController.cafe = cafe
      }
      if !IOS_8() {
        cafeDetailViewController.mainNavigationController = navigationController as? MainNavigationController
      }
    }
    navigationItem.backBarButtonItem = UIBarButtonItem (title: "", style: .Bordered, target: nil, action: nil)
  }
  
  func configure() {
    if type == .Search {
      return
    }
    let titleLabel = UILabel(frame: CGRect(x: -10, y: 0, width: 174, height: 44))
    titleLabel.text = "Guia de Cafeterias"
    titleLabel.font = UIFont(name: "Brix Slab", size: 22)
    let titleView = UILabel(frame: CGRect(x: 0, y: 0, width: 159, height: 44))
    titleView.addSubview(titleLabel)
    let title = UIBarButtonItem(customView: titleView)
    navigationItem.leftBarButtonItems = [title]
    let completion = { (var cafes: [Cafe]?) -> () in
      if cafes != nil {
        if self.type == .Favorites && cafes!.count == 0 {
          self.tableView.hidden = true
          let label = UILabel(frame: CGRect(x: 30, y: 0, width: self.view.bounds.width - 60, height: self.view.bounds.height))
          label.text = "Você ainda não possui nenhuma cafeteria favorita."
          label.font = UIFont(name: "ClioBold-Bold", size: 18)
          label.textAlignment = .Center
          label.numberOfLines = 0
          self.view.addSubview(label)
        } else if self.type == .Top20 || self.type == .Newcomers {
          cafes!.sort({ (lhs: Cafe, rhs: Cafe) -> Bool in
            if let location = (self.navigationController as! MainNavigationController).locationManager.location {
              return lhs.location.distanceFromLocation(location) < rhs.location.distanceFromLocation(location)
            }
            return true
          })
        }
        SVProgressHUD.dismiss()
        self.tableViewDelegate.datasource = cafes!
        self.tableView.reloadData()
      } else {
        SVProgressHUD.showErrorWithStatus("Erro de conexão.")
      }
    }
    let menuItem = UIBarButtonItem(image: UIImage(named: "icn_menu"), style: .Bordered, target: self, action: "showMenu")
    switch type {
    case .All:
      navigationItem.rightBarButtonItems = [
      menuItem,
      UIBarButtonItem(image: UIImage(named: "icn_map"), style: .Bordered, target: self, action: "showMap")]
      sortView = NSBundle.mainBundle().loadNibNamed("SortView", owner: self, options: nil).first as? SortView
      sortView!.frame = CGRect(x: 0, y: view.bounds.height - sortView!.frame.height, width: view.bounds.width, height: sortView!.bounds.height)
      sortView!.delegate = self
      view.addSubview(sortView!)
      tableViewDelegate.delegate = self
      loadPage(1)
    case .Top20:
      navigationItem.rightBarButtonItems = [menuItem]
      Cafe.getTop20Cafes(1, perPage: pageSize, completion: completion)
      SVProgressHUD.show()
      sortView = NSBundle.mainBundle().loadNibNamed("SortView", owner: self, options: nil).first as? SortView
      sortView!.frame = CGRect(x: 0, y: view.bounds.height - sortView!.frame.height, width: view.bounds.width, height: sortView!.bounds.height)
      sortView!.delegate = self
      view.addSubview(sortView!)
      tableViewDelegate.delegate = self
    case .Newcomers:
      navigationItem.rightBarButtonItems = [menuItem]
      Cafe.getNewcomersCafes(1, perPage: pageSize, completion: completion)
      SVProgressHUD.show()
      sortView = NSBundle.mainBundle().loadNibNamed("SortView", owner: self, options: nil).first as? SortView
      sortView!.frame = CGRect(x: 0, y: view.bounds.height - sortView!.frame.height, width: view.bounds.width, height: sortView!.bounds.height)
      sortView!.delegate = self
      view.addSubview(sortView!)
      tableViewDelegate.delegate = self
    case .Favorites:
      navigationItem.rightBarButtonItems = [menuItem]
      Cafe.getFavoritesCafes(1, perPage: pageSize, completion: completion)
      SVProgressHUD.show()
    default:
      break
    }
  }
  
  func showMap() {
    self.navigationController?.viewControllers = [storyboard!.instantiateViewControllerWithIdentifier("MapViewController")]
  }
  
  func showMenu() {
    if (navigationController as! MainNavigationController).menuViewController != nil {
      self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
    } else {
      self.navigationController?.performSegueWithIdentifier("SegueMenu", sender: nil)
    }
  }
  
  func goToSearch() {
    if let mainNavigationController = navigationController as? MainNavigationController {
      mainNavigationController.presentSearch()
    }
  }
  
  func loadPage(page: Int) {
    switch type {
    case .All:
      loadAllPage(page)
    case .Top20:
      loadTop20Page(page)
    case .Newcomers:
      loadNewcomersPage(page)
    case .Favorites:
      loadNewcomersPage(page)
    case .Search:
      loadSearchPage(page)
    }
  }
  
  func loadAllPage(page: Int) {
    let completion = { (cafes: [Cafe]) -> () in
      self.currentPage = page
      if page == 1 {
        self.tableViewDelegate.datasource.removeAll(keepCapacity: false)
        self.tableView.scrollRectToVisible(self.tableView.bounds, animated: false)
      }
      self.tableViewDelegate.datasource = self.tableViewDelegate.datasource + cafes
      SVProgressHUD.dismiss()
      self.loadingPage = nil
      self.tableView.reloadData()
    }
    loadingPage = page
    SVProgressHUD.show()
    if sortView?.segmentedControl.selectedSegmentIndex == 1 {
      Cafe.getAllCafesWithRatingAtPage(page, perPage: pageSize, completion: completion)
    } else {
      if (navigationController as! MainNavigationController).locationManager.location != nil {
        Cafe.getAllCafesWithUserLocation((navigationController as! MainNavigationController).locationManager.location, page: page, perPage: pageSize, completion: completion)
      }
    }
  }
  
  func loadTop20Page(page: Int) {
    let completion = { (cafes: [Cafe]?) -> () in
      if cafes != nil {
        self.tableViewDelegate.datasource += cafes!
        var sorting: (Cafe, Cafe) -> Bool = {
          if self.sortView?.segmentedControl.selectedSegmentIndex == 1 {
            return $0.rating >= $1.rating
          } else {
            if let navigationController = self.navigationController as? MainNavigationController {
              let distance1 = navigationController.userLocation?.distanceFromLocation($0.location)
              let distance2 = navigationController.userLocation?.distanceFromLocation($1.location)
              return distance1 <= distance2
            } else {
              return true
            }
          }
        }
        self.tableViewDelegate.datasource.sort(sorting)
        self.tableView.reloadData()
        SVProgressHUD.dismiss()
      } else {
        SVProgressHUD.showErrorWithStatus("Erro de conexão.")
      }
    }
    SVProgressHUD.show()
    Cafe.getTop20Cafes(page, perPage: pageSize, completion: completion)
  }
  
  func loadNewcomersPage(page: Int) {
    let completion = { (cafes: [Cafe]?) -> () in
      if cafes != nil {
        self.tableViewDelegate.datasource += cafes!
        var sorting: (Cafe, Cafe) -> Bool = {
          if self.sortView?.segmentedControl.selectedSegmentIndex == 1 {
            return $0.rating >= $1.rating
          } else {
            if let navigationController = self.navigationController as? MainNavigationController {
              let distance1 = navigationController.userLocation?.distanceFromLocation($0.location)
              let distance2 = navigationController.userLocation?.distanceFromLocation($1.location)
              return distance1 <= distance2
            } else {
              return true
            }
          }
        }
        self.tableViewDelegate.datasource.sort(sorting)
        self.tableView.reloadData()
        SVProgressHUD.dismiss()
      } else {
        SVProgressHUD.showErrorWithStatus("Erro de conexão.")
      }
    }
    SVProgressHUD.show()
    Cafe.getNewcomersCafes(page, perPage: pageSize, completion: completion)
  }
  
  func loadFavoritesPage(page: Int) {
    let completion = { (cafes: [Cafe]?) -> () in
      if cafes != nil {
        self.tableViewDelegate.datasource = self.tableViewDelegate.datasource + cafes!
        self.tableView.reloadData()
        SVProgressHUD.dismiss()
      } else {
        SVProgressHUD.showErrorWithStatus("Erro de conexão.")
      }
    }
    SVProgressHUD.show()
    Cafe.getFavoritesCafes(page, perPage: pageSize, completion: completion)
  }
  
  func loadSearchPage(page: Int) {
    let completion = { (cafes: [Cafe]?) -> () in
      if cafes != nil {
        self.tableViewDelegate.datasource = self.tableViewDelegate.datasource + cafes!
        self.tableView.reloadData()
        SVProgressHUD.dismiss()
      } else {
        SVProgressHUD.showErrorWithStatus("Erro de conexão.")
      }
    }
    SVProgressHUD.show()
    Cafe.getCafesWithTerm(searchTerm, features: searchFeatures, page: page, perPage: pageSize, completion: completion)
  }
}

extension CafeListViewController: CafeListTableViewDelegateDelegate {
  func tableViewDidScroll(tableView: UITableView) {
    if tableView.contentOffset.y >= tableView.contentSize.height - tableView.bounds.height * 1.5 {
      if loadingPage == nil {
        let numberOfSections: Int = tableView.numberOfSections()
        let numberOfRowsInSection0: Int = tableView.numberOfRowsInSection(0)
        let numberOfItems = numberOfRowsInSection0 + (numberOfSections > 1 ? tableView.numberOfRowsInSection(1) : 0)
        if numberOfItems % pageSize != 0 {
          return
        }
        let nextPage = numberOfItems / pageSize + 1
        if nextPage != currentPage {
          loadPage(nextPage)
        }
      }
    }
    if type != .All && type != .Top20 && type != .Newcomers {
      return
    }
    if tableView.contentOffset.y < -64 || tableView.contentOffset.y > (tableView.contentSize.height - tableView.bounds.height) {
      return
    }
    let deltaY = (tableView.contentOffset.y - lastOffset.y) / 2.0
    sortView!.frame = CGRect(x: 0, y: min(max(view.bounds.height - sortView!.bounds.height, sortView!.frame.origin.y + deltaY), view.bounds.height), width: sortView!.bounds.width, height: sortView!.bounds.height)
    lastOffset = tableView.contentOffset
  }
  
  func tableViewDidStop(tableView: UITableView) {
    var rect = CGRectZero
    if sortView!.frame.origin.y >= view.bounds.height - sortView!.bounds.height / 2 {
      rect = CGRect(x: 0, y: view.bounds.height, width: sortView!.bounds.width, height: sortView!.bounds.height)
    } else {
      rect = CGRect(x: 0, y: view.bounds.height - sortView!.bounds.height, width: sortView!.bounds.width, height: sortView!.bounds.height)
    }
    UIView.animateWithDuration(0.3, animations: { () -> Void in
      self.sortView!.frame = rect
    })
  }
  
  func removeDefavoritedCafes() {
    if type == .Favorites {
      if let indexPath = tableView.indexPathForSelectedRow() {
        let cafe = tableViewDelegate.datasource[indexPath.row]
        if !cafe.favorited {
          var cafes = tableViewDelegate.datasource
          cafes.removeAtIndex(indexPath.row)
          tableViewDelegate.datasource = cafes
          tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
          if cafes.count == 0 {
            tableView.hidden = true
            let label = UILabel(frame: CGRect(x: 30, y: 0, width: view.bounds.width - 60, height: view.bounds.height))
            label.text = "Você ainda não possui nenhuma cafeteria favorita."
            label.font = UIFont(name: "ClioBold-Bold", size: 18)
            label.textAlignment = .Center
            label.numberOfLines = 0
            view.addSubview(label)
          }
        }
      }
    }
  }
  
  func updateNewlyRatedCafe() {
    if let indexPath = tableView.indexPathForSelectedRow(), let cell = tableView.cellForRowAtIndexPath(indexPath) as? CafeCell {
      let cafe = tableViewDelegate.datasource[indexPath.row]
      cell.updateCafeRatingTo(cafe.rating, total: cafe.ratingCount)
    }
  }
}

extension CafeListViewController: SortViewDelegate {
  func didChangeSortRating(on: Bool) {
    switch type {
    case .Newcomers, .Top20:
      var sorting: (Cafe, Cafe) -> Bool = {
        if on {
          return $0.rating >= $1.rating
        } else {
          if let navigationController = self.navigationController as? MainNavigationController {
            let distance1 = navigationController.userLocation?.distanceFromLocation($0.location)
            let distance2 = navigationController.userLocation?.distanceFromLocation($1.location)
            return distance1 <= distance2
          } else {
            return true
          }
        }
      }
      tableViewDelegate.datasource.sort(sorting)
      tableView.reloadData()
    case .All:
      loadPage(1)
    default:
      break;
    }
  }
}
