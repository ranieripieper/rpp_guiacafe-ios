//
//  AboutViewController.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 1/22/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {
  @IBOutlet weak var textView: UITextView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    let items = [UIBarButtonItem(image: UIImage(named: "icn_menu"), style: .Bordered, target: self, action: "showMenu")]
    navigationItem.rightBarButtonItems = items
    let titleLabel = UILabel(frame: CGRect(x: -10, y: 0, width: 174, height: 44))
    titleLabel.text = "Guia de Cafeterias"
    titleLabel.font = UIFont(name: "Brix Slab", size: 22)
    let titleView = UILabel(frame: CGRect(x: 0, y: 0, width: 159, height: 44))
    titleView.addSubview(titleLabel)
    let title = UIBarButtonItem(customView: titleView)
    navigationItem.leftBarButtonItems = [title]
    
    let string = "O mais completo roteiro de cafeterias do País em seu celular.\n\nO Guia de Cafeterias do Brasil traz novidades para quem quer planejar um roteiro por estabelecimentos de todo o País, com as dicas de lugares que oferecem um ótimo serviço de café e também quitutes para acompanhar. Detalhado, o Guia apresenta a localização das cafeterias de forma clara e objetiva, divididas por Estado e cidade; a marca do café oferecido; quais máquinas e métodos de preparo são utilizados; além deinformações sobre serviços essenciais como acesso à internet via wi-fi no local; proximidade de pontos turísticos e estações de metrô e trem; e particularidades da cafeteria como presença de torrefação própria, o que pode conferir mais frescor ao café.\n\nO App traz, ainda, o Top 20, uma lista com as vinte melhores casas que se destacam pela tradição, pelo pioneirismo, ambiente, atendimento, e, principalmente, pela qualidade do café servido. Essas são as superindicadas pela equipe da publicação como autênticas representantes do que há de melhor no País no setor de café.\n\nTambém, de olho nas novidades, recomendamos as melhores cafeterias recém-abertas com o selo “Cafeterias Revelação”. Imperdível!\n\nPara dúvidas ou sugestões, entre em contato conosco: contato@cafeeditora.com.br"
    var attr = NSMutableAttributedString(string: string)
    attr.setAttributes([NSFontAttributeName: UIFont(name: "ClioBold-Bold", size: 18)!], range: NSMakeRange(0, 61))
    attr.setAttributes([NSFontAttributeName: UIFont(name: "ClioLight-Light", size: 18)!], range: NSMakeRange(62, count(string) - 62 - 26))
    attr.setAttributes([NSUnderlineStyleAttributeName: 1, NSFontAttributeName: UIFont(name: "ClioLight-Light", size: 18)!], range: NSMakeRange(count(string) - 26, 26))
    textView.attributedText = attr
    
    textView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
  }
  
  func showMenu() {
    if (navigationController as! MainNavigationController).menuViewController != nil {
      self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
    } else {
      self.navigationController?.performSegueWithIdentifier("SegueMenu", sender: nil)
    }
  }
}
