//
//  CafeDetailViewController.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 12/9/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit
import MapKit

class CafeDetailViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var favoriteBarButton: UIBarButtonItem!
  @IBOutlet weak var featuresAnimator: FeaturesAnimator!
  @IBOutlet weak var cafeDetailTableViewDelegate: CafeDetailTableViewDelegate!
  
  var cafe: Cafe?
  var alertView: AlertController?
  var rateView: RateView?
  var mainNavigationController: MainNavigationController?
  
  deinit {
    cafeDetailTableViewDelegate.delegate = nil
    tableView.delegate = nil
    tableView.dataSource = nil
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    refreshFavoriteButton(cafe!.favorited)
    cafeDetailTableViewDelegate.delegate = self
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if let featuresViewController = segue.destinationViewController as? FeaturesViewController {
      featuresViewController.transitioningDelegate = featuresAnimator
      if let features = sender as? [Feature] {
        featuresViewController.features = features
      }
      if !IOS_8() {
        featuresViewController.mainNavigationController = mainNavigationController
      }
    }
  }
  
  @IBAction func toggleFavorite(sender: UIBarButtonItem) {
    if let token = SSKeychain.passwordForService("guiacafeterias", account: "guiacafeterias") {
      cafe!.favorite(!cafe!.favorited) { success in
        if success {
          self.cafe!.favorited = !self.cafe!.favorited
          self.refreshFavoriteButton(self.cafe!.favorited)
        }
      }
    } else {
      alertView = AlertController(title: "Guia de Cafeterias", message: "Para favoritar, é preciso se logar!", buttons: [("Login", {
        self.goToLogin()
        })], cancelButton: ("Cancelar", nil), style: .Alert)
      alertView?.alertInViewController(self)
    }
  }
  
  func refreshFavoriteButton(on: Bool) {
    if on {
      favoriteBarButton.image = UIImage(named: "icn_fav_filled_detail")
    } else {
      favoriteBarButton.image = UIImage(named: "icn_fav_detail")
    }
  }
  
  func openWaze() {
    let url = "waze://?ll=\(cafe!.location.coordinate.latitude),\(cafe!.location.coordinate.longitude)&navigate=yes"
    UIApplication.sharedApplication().openURL(NSURL(string: url)!)
  }
  
  func openGoogleMaps() {
    let urlString = "comgooglemaps://?center=\(cafe!.location.coordinate.latitude),\(cafe!.location.coordinate.longitude)&zoom=16&views=traffic&q=\(cafe!.location.coordinate.latitude),\(cafe!.location.coordinate.longitude)"
    let url = NSURL(string: urlString.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!)
    if url != nil {
      UIApplication.sharedApplication().openURL(url!)
    }
  }
  
  func openMaps() {
    let place = MKPlacemark(coordinate: cafe!.location.coordinate, addressDictionary: nil)
    let mapItem = MKMapItem(placemark: place)
    mapItem.name = cafe!.name
    mapItem.openInMapsWithLaunchOptions(nil)
  }
  
  func rate() {
    if rateView != nil {
      return
    }
    if let token = SSKeychain.passwordForService("guiacafeterias", account: "guiacafeterias") {
      rateView = NSBundle.mainBundle().loadNibNamed("RateView", owner: self, options: nil).last as? RateView
      rateView!.callback = { ok, value in
        self.rateView!.dismissFromViewController(self)
        if ok {
          SVProgressHUD.show()
          self.cafe!.rate(value, completion: { (success) -> () in
            if success {
              self.cafe!.refreshCafeDetails() {
                let cell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: find(self.cafe!.details(), "INFO")!, inSection: 0)) as! CafeDetailInfoCell
                cell.updateRatingTo(self.cafe!.rating, total: self.cafe!.ratingCount)
              }
              SVProgressHUD.showSuccessWithStatus("Obrigado! Seu voto foi registrado.")
            } else {
              SVProgressHUD.showErrorWithStatus("Erro de conexão.")
            }
          })
        }
        self.rateView = nil
      }
      rateView!.presentInViewController(self)
    } else {
      alertView = AlertController(title: "Guia de Cafeterias", message: "Para avaliar, é preciso se logar!", buttons: [("Login", {
        self.goToLogin()
      })], cancelButton: ("Cancelar", nil), style: .Alert)
      alertView?.alertInViewController(self)
    }
  }
  
  func share() {
    var activityItems: [AnyObject] = ["Confira mais sobre a \(cafe!.name) no App Guia de Cafeterias http://itunes.apple.com/us/app/apple-store/id943052424?mt=8"]
    let activityViewController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
    presentViewController(activityViewController, animated: true, completion: nil)
  }
  
  func goToLogin() {
    let loginViewController = storyboard?.instantiateViewControllerWithIdentifier("LoginViewController") as! LoginViewController
    loginViewController.innerLogin = true
    if navigationController != nil {
      navigationController?.pushViewController(loginViewController, animated: true)
    } else {
      mainNavigationController?.pushViewController(loginViewController, animated: true)
    }
    navigationItem.backBarButtonItem = UIBarButtonItem (title: "", style: .Bordered, target: nil, action: nil)
  }
  
  private func canOpenAppWithURL(url: String) -> Bool {
    return UIApplication.sharedApplication().canOpenURL(NSURL(string: url)!)
  }
}

extension CafeDetailViewController: CafeDetailTableViewDelegateDelegate {
  func goToCafe() {
    let hasWaze = canOpenAppWithURL("waze://")
    let hasGoogleMaps = canOpenAppWithURL("comgooglemaps://")
    if hasWaze || hasGoogleMaps {
      alertView = AlertController(title: "Escolha um aplicativo", message: nil, buttons: [("Waze", {
        self.openWaze()
        self.alertView = nil
      }), ("Google Maps", {
        self.openGoogleMaps()
        self.alertView = nil
      }), ("Apple Maps", {
        self.openMaps()
        self.alertView = nil
      })], cancelButton: ("Cancelar", {
        self.alertView = nil
      }), style: .ActionSheet)
      alertView!.alertInViewController(self)
    } else {
      openMaps()
    }
  }
  
  func call() {
    alertView = AlertController(title: "Ligar para \(cafe!.name)", message: "Tel.: \(cafe!.contact.phone!)", buttons: [("Ligar", {
      let phone = (self.cafe!.contact.phone! as NSString).stringByReplacingOccurrencesOfString(" ", withString: "", options: .CaseInsensitiveSearch, range: NSMakeRange(0, count(self.cafe!.contact.phone!)))
      if let phoneUrl = NSURL(string: "tel://\(phone)") {
        UIApplication.sharedApplication().openURL(phoneUrl)
      }
      self.alertView = nil
    })], cancelButton: ("Cancelar", {
      self.alertView = nil
    }), style: .Alert)
    alertView!.alertInViewController(self)
  }
  
  func goToWebsite() {
    if cafe!.contact.website != nil {
      UIApplication.sharedApplication().openURL(NSURL(string: cafe!.contact.website!)!)
    }
  }
}
