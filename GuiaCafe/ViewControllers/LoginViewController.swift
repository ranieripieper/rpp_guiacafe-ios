//
//  SettingsViewController.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 12/9/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
  @IBOutlet weak var facebookLabel: UILabel!
  @IBOutlet weak var googlePlusLabel: UILabel!
  
  let session = Session()
  let connectFacebook = "CONECTAR COM FACEBOOK"
  let disconnectFacebook = "DESCONECTAR DO FACEBOOK"
  let connectGooglePlus = "CONECTAR COM GOOGLE+"
  let disconnectGooglePlus = "DESCONECTAR DO GOOGLE+"
  let signIn = GPPSignIn.sharedInstance()
  var innerLogin = false
  var alertView: AlertController?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configureView()
    signIn.shouldFetchGooglePlusUser = true
    signIn.shouldFetchGoogleUserEmail = true
    signIn.clientID = "430884674440-vjr6d4g8bumil0pol43k9s3aj4kb4841.apps.googleusercontent.com"
    signIn.scopes = [kGTLAuthScopePlusLogin]
    signIn.delegate = self
  }
  
  @IBAction func connectFacebook(sender: UIButton) {
    if session.isLoggedInToFacebook() {
      session.logoutOfFacebook()
      facebookLabel.text = connectFacebook
      if !session.isLoggedInToGooglePlus() {
        SSKeychain.deletePasswordForService("guiacafeterias", account: "guiacafeterias")
      }
    } else {
      session.loginWithFacebookWithClosure() { connected, error in
        if error != nil {
          self.facebookLabel.text = self.connectFacebook
          self.alertView = AlertController(title: "Guia de Cafeterias", message: error ?? "Não foi possível fazer login com o Facebook.", buttons: nil, cancelButton: ("Cancelar", {
            self.alertView = nil
          }), style: .Alert)
          self.alertView?.alertInViewController(self)
        } else {
          if connected {
            self.facebookLabel.text = self.disconnectFacebook
            if self.innerLogin {
              self.navigationController?.popViewControllerAnimated(true)
            }
          } else {
            self.facebookLabel.text = self.connectFacebook
          }
        }
      }
    }
  }
  
  @IBAction func connectGooglePlus(sender: UIButton) {
    if session.isLoggedInToGooglePlus() {
      session.logoutOfGooglePlus()
      signIn.signOut()
      googlePlusLabel.text = connectGooglePlus
    } else {
      if signIn.hasAuthInKeychain() {
        if !signIn.trySilentAuthentication() {
          signIn.authenticate()
        }
      } else {
        signIn.authenticate()
      }
    }
  }
  
  func showMenu() {
    if (navigationController as! MainNavigationController).menuViewController != nil {
      self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
    } else {
      self.navigationController?.performSegueWithIdentifier("SegueMenu", sender: nil)
    }
  }
  
  func configureView() {
    if !innerLogin {
      let items = [UIBarButtonItem(image: UIImage(named: "icn_menu"), style: .Bordered, target: self, action: "showMenu")]
      navigationItem.rightBarButtonItems = items
      let titleLabel = UILabel(frame: CGRect(x: -10, y: 0, width: 174, height: 44))
      titleLabel.text = "Guia de Cafeterias"
      titleLabel.font = UIFont(name: "Brix Slab", size: 22)
      let titleView = UILabel(frame: CGRect(x: 0, y: 0, width: 159, height: 44))
      titleView.addSubview(titleLabel)
      let title = UIBarButtonItem(customView: titleView)
      navigationItem.leftBarButtonItems = [title]
    }
    
    if session.isLoggedInToFacebook() {
      facebookLabel.text = disconnectFacebook
    }
    if session.isLoggedInToGooglePlus() {
      googlePlusLabel.text = disconnectGooglePlus
    }
  }
}

extension LoginViewController: GPPSignInDelegate {
  func finishedWithAuth(auth: GTMOAuth2Authentication!, error: NSError!) {
    if error == nil {
      session.loginWithEmail(auth.userEmail, name: signIn.googlePlusUser.displayName, source: .GooglePlus, auth_token: auth.accessToken) { (success) -> () in
        self.googlePlusLabel.text = self.disconnectGooglePlus
        NSUserDefaults.standardUserDefaults().setBool(true, forKey: "GooglePlusLogin")
        NSUserDefaults.standardUserDefaults().synchronize()
        if self.innerLogin {
          self.navigationController?.popViewControllerAnimated(true)
        }
      }
    } else {
      googlePlusLabel.text = connectGooglePlus
    }
  }
  
  func didDisconnectWithError(error: NSError!) {
    if error == nil {
      googlePlusLabel.text = connectGooglePlus
    } else {
      googlePlusLabel.text = disconnectGooglePlus
    }
  }
}
