//
//  SplashViewController.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 1/16/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {
  @IBOutlet weak var imageView: UIImageView!

  var timer: NSTimer?
  
  let duration = 3.0
  
  override func viewDidLoad() {
    super.viewDidLoad()
    if UIScreen.mainScreen().bounds.width == 375 {
      imageView.image = UIImage(named: "splash6")
    }
    timer = NSTimer.scheduledTimerWithTimeInterval(duration, target: self, selector: "dismiss", userInfo: nil, repeats: false)
  }
  
  func dismiss() {
    (UIApplication.sharedApplication().delegate as! AppDelegate).resumeMainApplicationFlow()
  }
}
