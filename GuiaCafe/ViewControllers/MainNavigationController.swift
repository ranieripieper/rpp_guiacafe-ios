//
//  MainNavigationController.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 12/9/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit
import MapKit

class MainNavigationController: UINavigationController {
  @IBOutlet weak var menuAnimator: MenuAnimator!

  weak var menuViewController: MenuViewController?
  let locationManager = CLLocationManager()
  var userLocation: CLLocation?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    view.addGestureRecognizer(menuAnimator.menuPanGesture)
    if IOS_8() {
      locationManager.requestWhenInUseAuthorization()
    }
    locationManager.delegate = self
    locationManager.startUpdatingLocation()
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if let menuViewController = segue.destinationViewController as? MenuViewController {
      menuViewController.transitioningDelegate = menuAnimator
      self.menuViewController = menuViewController
      self.menuViewController!.mainNavigationController = self
    }
  }
  
  func presentCafeList() {
    if !(self.viewControllers.first is CafeListViewController) || (self.viewControllers.first as! CafeListViewController).type != CafeListType.All {
      self.viewControllers = [self.storyboard!.instantiateViewControllerWithIdentifier("CafeListViewController")]
    }
  }
  
  func presentSearch() {
    if !(self.viewControllers.first is SearchViewController) {
      self.viewControllers = [self.storyboard!.instantiateViewControllerWithIdentifier("SearchViewController")]
    }
  }
  
  func presentTop20() {
    if !(self.viewControllers.first is CafeListViewController) || (self.viewControllers.first as! CafeListViewController).type != CafeListType.Top20 {
      let top20ViewController = self.storyboard!.instantiateViewControllerWithIdentifier("CafeListViewController") as! CafeListViewController
      top20ViewController.type = CafeListType.Top20
      self.viewControllers = [top20ViewController]
    }
  }
  
  func presentNewcomers() {
    if !(self.viewControllers.first is CafeListViewController) || (self.viewControllers.first as! CafeListViewController).type != CafeListType.Newcomers {
      let newcomersViewController = self.storyboard!.instantiateViewControllerWithIdentifier("CafeListViewController") as! CafeListViewController
      newcomersViewController.type = CafeListType.Newcomers
      self.viewControllers = [newcomersViewController]
    }
  }
  
  func presentFavorites() {
    if !(self.viewControllers.first is CafeListViewController) || (self.viewControllers.first as! CafeListViewController).type != CafeListType.Favorites {
      let favoritesViewController = self.storyboard!.instantiateViewControllerWithIdentifier("CafeListViewController") as! CafeListViewController
      favoritesViewController.type = CafeListType.Favorites
      self.viewControllers = [favoritesViewController]
    }
  }
  
  func presentAbout() {
    if !(self.viewControllers.first is AboutViewController) {
      self.viewControllers = [self.storyboard!.instantiateViewControllerWithIdentifier("AboutViewController")]
    }
  }
  
  func presentLogin() {
    if !(self.viewControllers.first is LoginViewController) {
      self.viewControllers = [self.storyboard!.instantiateViewControllerWithIdentifier("LoginViewController")]
    }
  }
}

extension MainNavigationController: MenuTableViewDelegateDelegate {
  func goToCafeList() {
    dismissViewControllerAnimated(true, completion: { () -> Void in
      self.presentCafeList()
    })
  }
  
  func goToSearch() {
    dismissViewControllerAnimated(true, completion: { () -> Void in
      self.presentSearch()
    })
  }
  
  func goToTop20() {
    dismissViewControllerAnimated(true, completion: { () -> Void in
      self.presentTop20()
    })
  }
  
  func goToNewcomers() {
    dismissViewControllerAnimated(true, completion: { () -> Void in
      self.presentNewcomers()
    })
  }
  
  func goToFavorites() {
    dismissViewControllerAnimated(true, completion: { () -> Void in
      self.presentFavorites()
    })
  }
  
  func goToAbout() {
    dismissViewControllerAnimated(true, completion: { () -> Void in
      self.presentAbout()
    })
  }
  
  func goToLogin() {
    dismissViewControllerAnimated(true, completion: { () -> Void in
      self.presentLogin()
    })
  }
}

extension MainNavigationController: CLLocationManagerDelegate {
  func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
    if let location = locations.last as? CLLocation {
      if userLocation == nil || userLocation!.distanceFromLocation(location) > 100 {
        userLocation = location
        if let mapViewController = viewControllers.first as? MapViewController {
          mapViewController.refreshCafes()
        } else if let cafeListViewController = viewControllers.first as? CafeListViewController {
          cafeListViewController.loadPage(1)
        }
      }
    }
  }
}
