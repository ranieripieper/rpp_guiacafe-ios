//
//  FeatureCell.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 12/11/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class FeatureCell: UICollectionViewCell {
  @IBOutlet weak var featureImageView: UIImageView!
  
  func configureWithImage(image: UIImage) {
    featureImageView.image = image
  }
}
