//
//  CafeDetailShareCell.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 12/10/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class CafeDetailShareCell: UITableViewCell {
  @IBOutlet weak var shareButton: UIButton!
  
  weak var cafeDetailViewController: CafeDetailViewController?
  
  @IBAction func shareTapped(sender: UIButton) {
    if let cafeDetailViewController = self.cafeDetailViewController {
      cafeDetailViewController.share()
    }
  }
}
