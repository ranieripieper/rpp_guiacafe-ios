//
//  MenuCell.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 12/10/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {
  @IBOutlet weak var titleLabel: UILabel!

  func configureWithMenuItem(menuItem: MenuItem) {
    titleLabel.text = textForItem(menuItem)
  }
}
