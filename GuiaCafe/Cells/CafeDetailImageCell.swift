//
//  CafeDetailImageCellTableViewCell.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 12/10/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

@objc protocol CafeDetailImageCellDelegate {
  func didScroll(scrollView: UIScrollView)
}

class CafeDetailImageCell: UITableViewCell {
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var pageControl: UIPageControl!
  @IBOutlet weak var scrollViewWidthConstraint: NSLayoutConstraint!
  
  var images: [UIImage]?
  weak var delegate: CafeDetailImageCellDelegate?
  
  deinit {
    scrollView.delegate = nil
    delegate = nil
  }
  
  func configureWithImages(images: [String], initialContentOffset: CGPoint?) {
    scrollViewWidthConstraint.constant = bounds.width
    layoutIfNeeded()
    if self.images != nil {
      return
    }
    images.reduce(0) { index, string -> Int in
      let imageView = UIImageView(frame: CGRect(x: CGFloat(index) * self.scrollView.bounds.width, y: 0, width: self.scrollView.bounds.width, height: self.scrollView.bounds.height))
      imageView.contentMode = .ScaleAspectFill
      imageView.setImageWithString(string, width: Int(375 * UIScreen.mainScreen().scale), height: Int(228 * UIScreen.mainScreen().scale)) { image in
        if self.images == nil {
          self.images = []
        }
        if image != nil {
          self.images!.append(image!)
        }
      }
      self.scrollView.addSubview(imageView)
      self.scrollView.contentSize = CGSize(width: CGFloat(index + 1) * self.scrollView.bounds.width, height: self.scrollView.bounds.height)
      self.pageControl.numberOfPages = index + 1
      return index + 1
    }
    if initialContentOffset != nil {
      scrollView.contentOffset = initialContentOffset!
    }
  }
}

extension CafeDetailImageCell: UIScrollViewDelegate {
  func scrollViewDidScroll(scrollView: UIScrollView) {
    pageControl.currentPage = Int(scrollView.contentOffset.x / scrollView.bounds.width + 0.5)
    delegate?.didScroll(scrollView)
  }
}
