//
//  CafeDetailDetailCell.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 12/10/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class CafeDetailDetailCell: UITableViewCell {
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var descriptionLabel: UILabel!
  
  var topLineView: UIView?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    let screenWidth = UIScreen.mainScreen().bounds.width
    titleLabel.preferredMaxLayoutWidth = screenWidth - 16.0
    descriptionLabel.preferredMaxLayoutWidth = screenWidth - 16.0
  }
  
  func configureWithTitle(title: String, titleColor: UIColor, description: String) {
    titleLabel.text = title
    titleLabel.textColor = titleColor
    let paragraph = NSMutableParagraphStyle()
    paragraph.lineSpacing = 6
    let attr = NSMutableAttributedString(string: description, attributes: [NSParagraphStyleAttributeName: paragraph])
    descriptionLabel.attributedText = attr
    if topLineView != nil {
      topLineView!.removeFromSuperview()
      topLineView = nil
    }
  }
  
  func addTopLine() {
    if topLineView != nil {
      return
    }
    topLineView = UIView(frame: CGRect(x: 0, y: 0, width: bounds.width, height: 1))
    topLineView!.backgroundColor = UIColor(red: 215.0/255, green: 215.0/255, blue: 215.0/255, alpha: 1.0)
    addSubview(topLineView!)
  }
}
