//
//  CafeCell.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 12/10/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class CafeCell: UITableViewCell {
  @IBOutlet weak var logoImageView: UIImageView!
  @IBOutlet weak var pinImageView: UIImageView!
  @IBOutlet weak var star1ImageView: UIImageView!
  @IBOutlet weak var star2ImageView: UIImageView!
  @IBOutlet weak var star3ImageView: UIImageView!
  @IBOutlet weak var star4ImageView: UIImageView!
  @IBOutlet weak var star5ImageView: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var establishedLabel: UILabel!
  @IBOutlet weak var distanceLabel: UILabel!
  @IBOutlet weak var ratingLabel: UILabel!
  @IBOutlet weak var serviceLabel: UILabel!
  @IBOutlet weak var serviceContentLabel: UILabel!
  @IBOutlet weak var stateLabel: UILabel!
  
  var request: Request?
  
  override func prepareForReuse() {
    logoImageView.setImageWithString(nil)
    super.prepareForReuse()
  }

  func configureWithCafe(cafe: Cafe, distance: Double?, imageCache: NSCache) {
    if request?.request.URLString != cafe.images.first {
      request?.cancel()
    }
    if cafe.images.first != nil {
      if let image = imageCache.objectForKey(cafe.images.first!) as? UIImage {
        logoImageView.image = image
      }
    }
    request = logoImageView.setImageWithString(cafe.images.first, width: Int(140 * UIScreen.mainScreen().scale), height: Int(100.0 * UIScreen.mainScreen().scale), completion: { (image) in
      if image != nil {
        imageCache.setObject(image!, forKey: cafe.images.first!)
      }
    })
    updateCafeRatingTo(cafe.rating, total: cafe.ratingCount)
    nameLabel.text = cafe.name
    establishedLabel.text = "desde \(cafe.establishment)"
    if distance != nil {
      distanceLabel.text = String(format: "%.1fkm", distance!)
      distanceLabel.textColor = cafe.state.info().1
      pinImageView.image = UIImage(named: cafe.state.info().3)
    } else {
      distanceLabel.text = ""
      pinImageView.image = nil
    }
    serviceLabel.textColor = cafe.state.info().1
    serviceContentLabel.text = cafe.service
  }
  
  func configureWithCafe(cafe: Cafe, colored: Bool, distance: Double?, imageCache: NSCache) {
    configureWithCafe(cafe, distance: distance, imageCache: imageCache)
    stateLabel.hidden = !colored
    let paragraph = NSMutableParagraphStyle()
    paragraph.firstLineHeadIndent = 10
    let attr = NSAttributedString(string: cafe.state.info().0, attributes: [NSForegroundColorAttributeName: UIColor.whiteColor(), NSParagraphStyleAttributeName: paragraph])
    stateLabel.attributedText = attr
    stateLabel.backgroundColor = cafe.state.info().1
  }
  
  func updateCafeRatingTo(value: Float, total: Int) {
    let starFilledImage = UIImage(named: "icn_rate_filled")
    let starImage = UIImage(named: "icn_rate")
    let starAlmostFilledImage = UIImage(named: "icn_rate_almost_filled")
    star1ImageView.image = value >= 0.75 ? starFilledImage : (value >= 0.25 ? starAlmostFilledImage : starImage)
    star2ImageView.image = value >= 1.75 ? starFilledImage : (value >= 1.25 ? starAlmostFilledImage : starImage)
    star3ImageView.image = value >= 2.75 ? starFilledImage : (value >= 2.25 ? starAlmostFilledImage : starImage)
    star4ImageView.image = value >= 3.75 ? starFilledImage : (value >= 3.25 ? starAlmostFilledImage : starImage)
    star5ImageView.image = value >= 4.75 ? starFilledImage : (value >= 4.25 ? starAlmostFilledImage : starImage)
    if total == 1 {
      ratingLabel.text = "(\(total) avaliação)"
    } else {
      ratingLabel.text = "(\(total) avaliações)"
    }
  }
}
