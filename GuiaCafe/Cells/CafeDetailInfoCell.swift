//
//  CafeDetailInfoCell.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 12/10/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class CafeDetailInfoCell: UITableViewCell {
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var franchiseLabel: UILabel!
  @IBOutlet weak var distanceLabel: UILabel!
  @IBOutlet weak var establishmentLabel: UILabel!
  @IBOutlet weak var ratingLabel: UILabel!
  @IBOutlet weak var distanceImageView: UIImageView!
  @IBOutlet weak var premiumImageView: UIImageView!
  @IBOutlet weak var top20ImageView: UIImageView!
  @IBOutlet weak var newcomerImageView: UIImageView!
  @IBOutlet weak var star1ImageView: UIImageView!
  @IBOutlet weak var star2ImageView: UIImageView!
  @IBOutlet weak var star3ImageView: UIImageView!
  @IBOutlet weak var star4ImageView: UIImageView!
  @IBOutlet weak var star5ImageView: UIImageView!
  @IBOutlet weak var rateButton: UIButton!
  @IBOutlet weak var franchiseLabelHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var premiumWidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var top20WidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var newcomerWidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var ratingStarsTopSpaceConstraint: NSLayoutConstraint!
  
  weak var cafeDetailViewController: CafeDetailViewController?
  
  func configureWithCafe(cafe: Cafe, distance: Double?) {
    nameLabel.text = cafe.name
    franchiseLabel.text = cafe.franchise
    if distance != nil {
      distanceLabel.text = String(format: "%.1fkm", distance!)
      distanceImageView.image = UIImage(named: cafe.state.info().3)
      distanceLabel.textColor = cafe.state.info().1
    } else {
      distanceLabel.text = ""
      distanceImageView.image = nil
    }
    establishmentLabel.text = "desde \(cafe.establishment)"
    premiumImageView.hidden = !cafe.premium
    top20ImageView.hidden = !cafe.top20
    newcomerImageView.hidden = !cafe.newcomer
    franchiseLabelHeightConstraint.constant = count(cafe.franchise) > 0 ? 20 : 0
    premiumWidthConstraint.constant = cafe.premium ? 60 : 0
    top20WidthConstraint.constant = cafe.top20 ? 60 : 0
    newcomerWidthConstraint.constant = cafe.newcomer ? 60 : 0
    ratingStarsTopSpaceConstraint.constant = cafe.premium || cafe.top20 || cafe.newcomer ? 66 : 12
    updateRatingTo(cafe.rating, total: cafe.ratingCount)
  }
  
  @IBAction func rateTapped(sender: UIButton) {
    cafeDetailViewController?.rate()
  }
  
  func updateRatingTo(value: Float, total: Int) {
    let starImage = UIImage(named: "icn_rate_detail")
    let starFilledImage = UIImage(named: "icn_rate_filled_detail")
    let starAlmostFilledImage = UIImage(named: "icn_rate_almost_filled_detail")
    star1ImageView.image = value >= 0.75 ? starFilledImage : (value >= 0.25 ? starAlmostFilledImage : starImage)
    star2ImageView.image = value >= 1.75 ? starFilledImage : (value >= 1.25 ? starAlmostFilledImage : starImage)
    star3ImageView.image = value >= 2.75 ? starFilledImage : (value >= 2.25 ? starAlmostFilledImage : starImage)
    star4ImageView.image = value >= 3.75 ? starFilledImage : (value >= 3.25 ? starAlmostFilledImage : starImage)
    star5ImageView.image = value >= 4.75 ? starFilledImage : (value >= 4.25 ? starAlmostFilledImage : starImage)
    if total == 1 {
      ratingLabel.text = "(\(total) avaliação)"
    } else {
      ratingLabel.text = "(\(total) avaliações)"
    }
  }
}
