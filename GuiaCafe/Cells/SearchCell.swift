//
//  SearchCell.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 12/10/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class SearchCell: UITableViewCell {
  @IBOutlet weak var iconImageView: UIImageView!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var onOffButton: UIButton!
  
  var searchItem: SearchItem?
  
  func configureWithSearchItem(searchItem: SearchItem) {
    self.searchItem = searchItem
    iconImageView.image = UIImage(named: searchItem.feature.searchIcon)
    let name = searchItem.feature.name
    if count(name) > 0 {
      let swiftRange = name.startIndex..<advance(name.startIndex, 1)
      titleLabel.text = name.lowercaseString.stringByReplacingCharactersInRange(swiftRange, withString: name.substringWithRange(swiftRange).uppercaseString).stringByReplacingOccurrencesOfString("tnt", withString: "TNT", options: NSStringCompareOptions.LiteralSearch, range: name.rangeOfString(name))
      onOffButton.selected = searchItem.mode
    }
  }
  
  @IBAction func toggleItem(sender: UIButton) {
    sender.selected = !sender.selected
    if searchItem != nil {
      searchItem!.mode = sender.selected
    }
  }
}
