//
//  CafeDetailFeaturesCell.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 12/10/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

@objc protocol CafeDetailFeaturesCellDelegate {
  func didSelectFeature(feature: Feature)
}

class CafeDetailFeaturesCell: UITableViewCell {
  @IBOutlet weak var collectionView: UICollectionView!
  
  weak var delegate: CafeDetailFeaturesCellDelegate?
  var features: [Feature]?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    collectionView.frame = CGRect(origin: collectionView.frame.origin, size: CGSize(width: UIScreen.mainScreen().bounds.width, height: collectionView.bounds.height))
  }
  
  func configureWithFeatures(features: [Feature]) {
    self.features = features.sorted() { feature1, feature2 -> Bool in
      feature1.order < feature2.order
    }
    collectionView.reloadData()
  }
}

extension CafeDetailFeaturesCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return features?.count ?? 0
  }
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    let featureCell = collectionView.dequeueReusableCellWithReuseIdentifier("FeatureCell", forIndexPath: indexPath) as! FeatureCell
    featureCell.configureWithImage(UIImage(named: features![indexPath.item].icon)!)
    return featureCell
  }
  
  func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) -> () {
    delegate?.didSelectFeature(features![indexPath.item])
  }
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
    let width = UIScreen.mainScreen().bounds.width / 8.0
    return CGSize(width: width, height: width)
  }
}
