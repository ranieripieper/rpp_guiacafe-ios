//
//  CafeDetailServiceCell.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 12/10/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

@objc protocol CafeDetailServiceCellDelegate {
  func didSelectFacility(facility: Feature)
  func didTapAddress()
  func didTapPhone()
  func didTapWebsite()
}

class CafeDetailServiceCell: UITableViewCell {
  @IBOutlet weak var addressButton: UIButton!
  @IBOutlet weak var phoneButton: UIButton!
  @IBOutlet weak var websiteButton: UIButton!
  @IBOutlet weak var serviceLabel: UILabel!
  @IBOutlet weak var workingHoursLabel: UILabel!
  @IBOutlet weak var sittingLabel: UILabel!
  @IBOutlet weak var cardsLabel: UILabel!
  @IBOutlet weak var ticketsLabel: UILabel!
  @IBOutlet weak var cardsImageView: UIImageView!
  @IBOutlet weak var ticketsImageView: UIImageView!
  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var cardsImageViewHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var ticketsImageViewHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var cardsLabelHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var ticketsLabelHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var websiteButtonHeightConstraint: NSLayoutConstraint!
  
  weak var delegate: CafeDetailServiceCellDelegate?
  var facilities: [Feature]?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    addressButton.titleLabel?.numberOfLines = 0
  }
  
  func configureWithColor(titleColor: UIColor, address: String, workingHours: String, phone: String, sitting: String, website: String, cards: String, tickets: String, facilities: [Feature]) {
    serviceLabel.textColor = titleColor
    let attributes = [NSFontAttributeName: UIFont(name: "ClioBold-Bold", size: 12)!, NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue]
    addressButton.setTitle(address, forState: .Normal)
    addressButton.setTitleColor(titleColor, forState: .Normal)
    workingHoursLabel.text = workingHours
    phoneButton.setTitle("Tel.: \(phone)", forState: .Normal)
    sittingLabel.text = count(sitting) > 0 ? "\(sitting) lugares" : ""
    websiteButton.setTitle(website, forState: .Normal)
    websiteButtonHeightConstraint.constant = count(website) > 0 ? 16 : 0
    cardsLabel.text = cards
    ticketsLabel.text = tickets
    self.facilities = facilities.sorted() { feature1, feature2 -> Bool in
      feature1.order < feature2.order
    }
    cardsImageViewHeightConstraint.constant = count(cards) > 0 ? 23 : 0
    ticketsImageViewHeightConstraint.constant = count(tickets) > 0 ? 23 : 0
    cardsLabelHeightConstraint.constant = count(cards) > 0 ? 16 : 0
    ticketsLabelHeightConstraint.constant = count(tickets) > 0 ? 16 : 0
    collectionView.reloadData()
  }
  
  @IBAction func goToCafe(sender: UIButton) {
    delegate?.didTapAddress()
  }
  
  @IBAction func call(sender: UIButton) {
    delegate?.didTapPhone()
  }
  
  @IBAction func goToWebsite(sender: UIButton) {
    delegate?.didTapWebsite()
  }
}

extension CafeDetailServiceCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return facilities?.count ?? 0
  }
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    let featureCell = collectionView.dequeueReusableCellWithReuseIdentifier("FeatureCell", forIndexPath: indexPath) as! FeatureCell
    featureCell.configureWithImage(UIImage(named: facilities![indexPath.row].icon)!)
    return featureCell
  }
  
  func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
    delegate?.didSelectFacility(facilities![indexPath.item])
  }
}
