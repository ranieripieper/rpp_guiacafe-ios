//
//  FeaturesCell.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 12/11/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class FeaturesCell: UITableViewCell {
  @IBOutlet weak var featureImageView: UIImageView!
  @IBOutlet weak var featureTitleLabel: UILabel!
  
  func configureWithFeature(feature: Feature) {
    featureImageView.image = UIImage(named: feature.icon)
    let swiftRange = feature.name.startIndex..<advance(feature.name.startIndex, 1)
    featureTitleLabel.text = feature.name.lowercaseString.stringByReplacingCharactersInRange(swiftRange, withString: feature.name.substringWithRange(swiftRange).uppercaseString).stringByReplacingOccurrencesOfString("tnt", withString: "TNT", options: NSStringCompareOptions.LiteralSearch, range: feature.name.rangeOfString(feature.name))
  }
}
