//
//  MenuAnimator.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 12/9/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class MenuAnimator: UIPercentDrivenInteractiveTransition, UIViewControllerAnimatedTransitioning {
  @IBOutlet weak var navigationController: UINavigationController!
  @IBOutlet weak var menuPanGesture: UIPanGestureRecognizer!
  
  var presenting = false
  var interactive = false
  var fromViewControllerSuperview: UIView?
  let showingPercentage: CGFloat = 0.7
  
  func transitionDuration(transitionContext: UIViewControllerContextTransitioning) -> NSTimeInterval {
    return 0.3
  }
  
  func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
    if presenting {
      let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!
      let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
      let containerView = transitionContext.containerView()
      
      containerView.addSubview(toViewController.view)
      fromViewControllerSuperview = fromViewController.view.superview
      containerView.addSubview(fromViewController.view)
      
      UIView.animateWithDuration(transitionDuration(transitionContext), animations: {
        fromViewController.view.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, -self.showingPercentage * fromViewController.view.bounds.width, 0)
      }) { finished in
        if transitionContext.transitionWasCancelled() {
          (fromViewController as! MainNavigationController).menuViewController = nil
        } else {
          (fromViewController as! MainNavigationController).viewControllers.map() { vc -> () in
            if let viewC = vc as? UIViewController {
              viewC.view.userInteractionEnabled = false
            }
          }
        }
        self.fromViewControllerSuperview?.addSubview(fromViewController.view)
        transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
      }
    } else {
      let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!
      let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
      let containerView = transitionContext.containerView()
      
      UIView.animateWithDuration(transitionDuration(transitionContext), animations: {
        toViewController.view.transform = CGAffineTransformIdentity
      }) { finished in
        if !transitionContext.transitionWasCancelled() {
          self.fromViewControllerSuperview?.addSubview(toViewController.view)
          (toViewController as! MainNavigationController).viewControllers.map() { vc -> () in
            if let viewC = vc as? UIViewController {
              viewC.view.userInteractionEnabled = true
            }
          }
          (toViewController as! MainNavigationController).menuViewController = nil
        }
        transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
      }
    }
  }
  
  @IBAction func menuOpenPanGestureRecognizer(menuPanGestureRecognizer: UIPanGestureRecognizer) {
    let view = navigationController.view
    if menuPanGestureRecognizer.state == .Began {
      if navigationController.viewControllers.count > 1 {
        return
      }
      let location = menuPanGestureRecognizer.locationInView(view)
      if location.x > view.bounds.width * showingPercentage && view.frame.origin.x == 0 {
        interactive = true
        navigationController.performSegueWithIdentifier("SegueMenu", sender: nil)
      } else if view.frame.origin.x < 0 {
        interactive = true
        navigationController.dismissViewControllerAnimated(true, completion: nil)
      }
    } else if menuPanGestureRecognizer.state == .Changed {
      let translation = menuPanGestureRecognizer.translationInView(view)
      let d = fabs(translation.x / CGRectGetWidth(view.bounds))
      updateInteractiveTransition(d)
    } else if menuPanGestureRecognizer.state == .Ended {
      if (menuPanGestureRecognizer.velocityInView(view).x < 0 && presenting) || (menuPanGestureRecognizer.velocityInView(view).x > 0 && !presenting) {
        finishInteractiveTransition()
      } else {
        cancelInteractiveTransition()
      }
      interactive = false
    }
  }
}

extension MenuAnimator: UIViewControllerTransitioningDelegate {
  func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    self.presenting = true
    return self
  }
  
  func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    presenting = false
    return self
  }
  
  func interactionControllerForPresentation(animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
    presenting = true
    return interactive ? self : nil
  }
  
  func interactionControllerForDismissal(animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
    presenting = false
    return interactive ? self : nil
  }
}
