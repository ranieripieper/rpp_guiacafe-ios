//
//  FeaturesAnimator.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 12/11/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class FeaturesAnimator: NSObject, UIViewControllerAnimatedTransitioning {
  var presenting = false
  var dimmingView: UIView?
  
  func transitionDuration(transitionContext: UIViewControllerContextTransitioning) -> NSTimeInterval {
    return 0.3
  }
  
  func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
    if presenting {
      let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
      let containerView = transitionContext.containerView()
      
      dimmingView = UIView(frame: containerView.bounds)
      dimmingView?.backgroundColor = UIColor.blackColor()
      dimmingView?.alpha = 0.0
      
      containerView.addSubview(dimmingView!)
      containerView.addSubview(toViewController.view)
      toViewController.view.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01)
      
      UIView.animateWithDuration(transitionDuration(transitionContext), delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
        self.dimmingView?.alpha = 0.7
        toViewController.view.transform = CGAffineTransformIdentity
      }, completion: { finished in
        transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
      })
    } else {
      let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!
      
      UIView.animateWithDuration(transitionDuration(transitionContext), animations: {
        fromViewController.view.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01)
        self.dimmingView?.alpha = 0.0
      }) { finished in
        transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
        self.dimmingView?.removeFromSuperview()
        self.dimmingView = nil
      }
    }
  }
}

extension FeaturesAnimator: UIViewControllerTransitioningDelegate {
  func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    self.presenting = true
    return self
  }
  
  func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    presenting = false
    return self
  }
}
