//
//  ImageAPI.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 1/20/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

extension UIImageView {
  func setImageWithString(string: String?) {
    setImageWithString(string, width: nil, height: nil, completion: nil)
  }
  
  func setImageWithString(var string: String?, width: Int?, height: Int?, completion: ((UIImage?) -> ())?) -> Request? {
    let placeholder = "img_place_holder"
    if string == nil || string == placeholder {
      image = UIImage(named: string ?? placeholder)
      if completion != nil {
        completion!(nil)
      }
      return nil
    }
    if image == nil {
      image = UIImage(named: placeholder)
    }
    if width != nil && height != nil {
      string = "http://guiadecafeterias.com.br/images/thumb.php?src=" + string! + "&w=\(width!)&h=\(height!)"
    }
    let request = Manager.sharedInstance.request(.GET, string!.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!, parameters: nil, encoding: .URL)
      .response { request, response, data, error in
        if data != nil && data is NSData && response?.statusCode == 200 {
          self.image = UIImage(data: data as! NSData)
          if completion != nil && self.image != nil {
            completion!(self.image!)
          }
        } else {
          self.image = UIImage(named: placeholder)
          if completion != nil {
            completion!(nil)
          }
        }
    }
    return request
  }
}

extension UIButton {
  func setImageWithString(string: String?) {
    setImageWithString(string, width: nil, height: nil, completion: nil)
  }
  
  func setImageWithString(var string: String?, width: Int?, height: Int?, completion: ((UIImage?) -> ())?) -> Request? {
    let placeholder = "img_place_holder"
    if string == nil || string == placeholder {
      setBackgroundImage(UIImage(named: string ?? placeholder), forState: .Normal)
      if completion != nil {
        completion!(nil)
      }
      return nil
    }
    if backgroundImageForState(.Normal) == nil {
      setBackgroundImage(UIImage(named: placeholder), forState: .Normal)
    }
    if width != nil && height != nil {
      string = "http://guiadecafeterias.com.br/images/thumb.php?src=" + string! + "&w=\(width!)&h=\(height!)"
    }
    let request = Manager.sharedInstance.request(.GET, string!.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!, parameters: nil, encoding: .URL)
      .response { request, response, data, error in
        if data != nil && data is NSData && response?.statusCode == 200 {
          let image = UIImage(data: data as! NSData)
          self.setBackgroundImage(image, forState: .Normal)
          if completion != nil {
            completion!(image)
          }
        } else {
          self.setBackgroundImage(UIImage(named: placeholder), forState: .Normal)
          if completion != nil {
            completion!(nil)
          }
        }
    }
    return request
  }
}
