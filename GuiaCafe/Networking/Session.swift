//
//  Session.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 2/9/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation
import Social
import Accounts

enum LoginSource {
  case Facebook
  case GooglePlus
}

class Session {
  let accountStore = ACAccountStore()
  
  // MARK: _PUBLIC_
  // MARK: Is logged in
  class func isLoggedIn() -> Bool {
    let session = Session()
    return session.isLoggedInToFacebook() || session.isLoggedInToGooglePlus()
  }
  
  func isLoggedInToFacebook() -> Bool {
    return NSUserDefaults.standardUserDefaults().boolForKey("FacebookLogin")
  }
  
  func isLoggedInToGooglePlus() -> Bool {
    return NSUserDefaults.standardUserDefaults().boolForKey("GooglePlusLogin")
  }
  
  // MARK: Login
  func loginWithFacebookWithClosure(closure: ((Bool, String?) -> ())?) {
    let facebookAccountType = accountStore.accountTypeWithAccountTypeIdentifier(ACAccountTypeIdentifierFacebook)
    let options = [ACFacebookAppIdKey: "1580831572154286", ACFacebookAudienceKey: ACFacebookAudienceEveryone, ACFacebookPermissionsKey: ["email"]]
    accountStore.requestAccessToAccountsWithType(facebookAccountType, options: options as [NSObject : AnyObject]) { (granted, error) in
      if granted {
        if let facebookAccount = self.accountStore.accountsWithAccountType(facebookAccountType).last as? ACAccount, let properties = facebookAccount.valueForKey("properties") as? NSDictionary, let email = properties.valueForKey("ACUIDisplayUsername") as? String, let name = properties.valueForKey("ACPropertyFullName") as? String, let auth_token = facebookAccount.credential.oauthToken {
          self.loginWithEmail(email, name: name, source: .Facebook, auth_token: auth_token) { success in
            if success {
              NSUserDefaults.standardUserDefaults().setBool(true, forKey: "FacebookLogin")
              NSUserDefaults.standardUserDefaults().synchronize()
            }
            closure?(success, nil)
          }
          return
        }
      } else if error.code == 6 {
        FBSession.openActiveSessionWithReadPermissions(["email"], allowLoginUI: true) { session, state, error in
          dispatch_async(dispatch_get_main_queue()) {
            if error != nil {
              closure?(false, error.localizedDescription)
            } else {
              if state == .ClosedLoginFailed {
                closure?(false, "Não foi possível fazer login com o Facebook.")
              } else {
                FBRequest.requestForMe().startWithCompletionHandler() { connection, user, error in
                  if error != nil {
                    closure?(false, "Não foi possível fazer login com o Facebook.")
                  } else {
                    self.loginWithEmail(user["email"] as! String, name: user.first_name, source: .Facebook, auth_token: FBSession.activeSession().accessTokenData.accessToken) { success in
                      if success {
                        NSUserDefaults.standardUserDefaults().setBool(true, forKey: "FacebookLogin")
                        NSUserDefaults.standardUserDefaults().synchronize()
                      }
                      closure?(success, nil)
                    }
                  }
                }
              }
            }
          }
        }
      } else {
        closure?(false, "Não foi possível fazer login com o Facebook.")
      }
    }
  }
  
  // MARK: Logout
  func logoutOfFacebook() {
    NSUserDefaults.standardUserDefaults().removeObjectForKey("FacebookLogin")
    NSUserDefaults.standardUserDefaults().synchronize()
    if !isLoggedInToGooglePlus() {
      SSKeychain.deletePasswordForService("guiacafeterias", account: "guiacafeterias")
    }
  }
  
  func logoutOfGooglePlus() {
    NSUserDefaults.standardUserDefaults().removeObjectForKey("GooglePlusLogin")
    NSUserDefaults.standardUserDefaults().synchronize()
    if !isLoggedInToFacebook() {
      SSKeychain.deletePasswordForService("guiacafeterias", account: "guiacafeterias")
    }
  }
  
  // MARK: Login API
  func loginWithEmail(email: String, name: String, source: LoginSource, auth_token: String, closure: ((Bool) -> ())?) {
    let params = ["name": name, "email": email, "source": (source == .Facebook ? "facebook" : "google"), "sourceToken": auth_token]
    Manager.sharedInstance
      .request(.POST, Router.Login, parameters: params, encoding: ParameterEncoding.URL)
      .responseJSON(completionHandler: { request, response, data, error in
        if let aData = data as? [String: AnyObject], let dataArray = aData["data"] as? [[String: String]], let dict = dataArray.first, let accessToken = dict["accessToken"] {
          SSKeychain.setPassword(accessToken, forService: "guiacafeterias", account: "guiacafeterias")
        }
        closure?(true)
    })
  }
}
