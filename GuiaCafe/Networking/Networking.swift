//
//  Networking.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 1/13/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

enum Router: URLStringConvertible {
  static let baseURLString = "http://guiadecafeterias.com.br/api/"
  
  case Root
  case Location(Double, Double, Int, Int)
  case Rating(Int, Int)
  case Top20(Int, Int)
  case Revelation(Int, Int)
  case GetFavorites(Int, Int)
  case Search(String?, [String]?, Int, Int)
  case Favorite(Bool, String)
  case Rate(String)
  case Login
  case Cafe(String)
  
  // MARK: URLStringConvertible
  var URLString: String {
    let path: String = {
      switch self {
        case .Root:
          return "places/"
        case .Location(let latitude, let longitude, let page, let perPage):
          return "places/query.json?geolocation[]=\(longitude)&geolocation[]=\(latitude)&page=\(page)&perPage=\(perPage)"
        case .Rating(let page, let perPage):
          return "places/query.json?orderBy=ratings.average&order=DESC&page=\(page)&perPage=\(perPage)"
        case .Top20(let page, let perPage):
          return "places/query.json?top20=1&page=\(page)&perPage=\(perPage)"
        case .Revelation(let page, let perPage):
          return "places/query.json?cafeteriaRevelacao=1&page=\(page)&perPage=\(perPage)"
        case .GetFavorites(let page, let perPage):
          return "favorites/list.json?page=\(page)&perPage=\(perPage)"
        case .Search(let string, let features, let page, let perPage):
          var query = "places/query.json?"
          if string != nil && count(string!) > 0 {
            query += "search=\(string!)"
          }
          if features != nil && features!.count > 0 {
            query = features!.reduce(query, combine: { (accumulated, string) -> String in
              accumulated + "&filters[]=" + string
            })
          }
          query + "&page=\(page)&perPage=\(perPage)"
          return query
        case .Favorite(let favorite, let uuid):
          if favorite {
            return "favorites/\(uuid)/add.json"
          } else {
            return "favorites/\(uuid)/remove.json"
          }
        case .Rate(let uuid):
          return "places/\(uuid)/rate.json"
        case .Login:
          return "oauth/connect.json"
        case .Cafe(let uuid):
          return "places/\(uuid).json"
        }
    }()
    return Router.baseURLString + path
  }
}
