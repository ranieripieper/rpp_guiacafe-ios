//
//  State.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 12/10/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import Foundation
import UIKit

typealias StateInfo = (String, UIColor, String, String)

enum State {
  case AC
  case AL
  case AP
  case AM
  case BA
  case CE
  case DF
  case ES
  case GO
  case MA
  case MS
  case MT
  case MG
  case PA
  case PB
  case PR
  case PE
  case PI
  case RN
  case RS
  case RJ
  case RO
  case RR
  case SC
  case SP
  case SE
  case TO
  case None
  
  func info() -> StateInfo {
    return (nameWithState(self), colorWithState(self), mapPinImageWithState(self), pinImageWithState(self))
  }
  
  init(string: String) {
    switch string.lowercaseString {
      case "acre", "ac":
        self = .AC
      case "alagoas", "al":
        self = .AL
      case "amapa", "amapá", "ap":
        self = .AP
      case "amazonas", "am":
        self = .AM
      case "bahia", "ba":
        self = .BA
      case "ceara", "ceará", "ce":
        self = .CE
      case "distrito federal", "df":
        self = .DF
      case "espirito santo", "espírito santo", "es":
        self = .ES
      case "goias", "goiás", "go":
        self = .GO
      case "maranhao", "maranhão", "ma":
        self = .MA
      case "mato grosso do sul", "ms":
        self = .MS
      case "mato grosso", "mt":
        self = .MT
      case "minas gerais", "mg":
        self = .MG
      case "para", "pará", "pa":
        self = .PA
      case "paraíba", "paraiba", "pb":
        self = .PB
      case "parana", "paraná", "pr":
        self = .PR
      case "pernambuco", "pe":
        self = .PE
      case "piaui", "piauí", "pi":
        self = .PI
      case "rio grande do norte", "rn":
        self = .RN
      case "rio grande do sul", "rs":
        self = .RS
      case "rio de janeiro", "rj":
        self = .RJ
      case "rondonia", "rondônia", "ro":
        self = .RO
      case "roraima", "rr":
        self = .RR
      case "santa catarina", "sc":
        self = .SC
      case "sao paulo", "são paulo", "sp":
        self = .SP
      case "sergipe", "se":
        self = .SE
      case "tocantins", "to":
        self = .TO
    default:
      self = .None
    }
  }
  
  private func colorWithState(state: State) -> UIColor {
    switch state {
    case .AC:
      return UIColor(red: 0.0/255.0, green: 55.0/255.0, blue: 66.0/255.0, alpha: 1.0)
    case .AL:
      return UIColor(red: 0.0/255.0, green: 93.0/255.0, blue: 92.0/255.0, alpha: 1.0)
    case .AP:
      return UIColor(red: 0.0/255.0, green: 114.0/255.0, blue: 83.0/255.0, alpha: 1.0)
    case .AM:
      return UIColor(red: 0.0/255.0, green: 132.0/255.0, blue: 126.0/255.0, alpha: 1.0)
    case .BA:
      return UIColor(red: 144.0/255.0, green: 186.0/255.0, blue: 53.0/255.0, alpha: 1.0)
    case .CE:
      return UIColor(red: 215.0/255.0, green: 200.0/255.0, blue: 0.0/255.0, alpha: 1.0)
    case .DF:
      return UIColor(red: 249.0/255.0, green: 157.0/255.0, blue: 28.0/255.0, alpha: 1.0)
    case .ES:
      return UIColor(red: 242.0/255.0, green: 101.0/255.0, blue: 34.0/255.0, alpha: 1.0)
    case .GO:
      return UIColor(red: 162.0/255.0, green: 91.0/255.0, blue: 22.0/255.0, alpha: 1.0)
    case .MA:
      return UIColor(red: 156.0/255.0, green: 67.0/255.0, blue: 21.0/255.0, alpha: 1.0)
    case .MS:
      return UIColor(red: 74.0/255.0, green: 20.0/255.0, blue: 53.0/255.0, alpha: 1.0)
    case .MT:
      return UIColor(red: 106.0/255.0, green: 56.0/255.0, blue: 0.0/255.0, alpha: 1.0)
    case .MG:
      return UIColor(red: 88.0/255.0, green: 89.0/255.0, blue: 91.0/255.0, alpha: 1.0)
    case .PA:
      return UIColor(red: 128.0/255.0, green: 130.0/255.0, blue: 133.0/255.0, alpha: 1.0)
    case .PB:
      return UIColor(red: 0.0/255.0, green: 45.0/255.0, blue: 97.0/255.0, alpha: 1.0)
    case .PR:
      return UIColor(red: 25.0/255.0, green: 69.0/255.0, blue: 157.0/255.0, alpha: 1.0)
    case .PE:
      return UIColor(red: 0.0/255.0, green: 129.0/255.0, blue: 200.0/255.0, alpha: 1.0)
    case .PI:
      return UIColor(red: 0.0/255.0, green: 154.0/255.0, blue: 188.0/255.0, alpha: 1.0)
    case .RN:
      return UIColor(red: 135.0/255.0, green: 129.0/255.0, blue: 189.0/255.0, alpha: 1.0)
    case .RS:
      return UIColor(red: 199.0/255.0, green: 107.0/255.0, blue: 171.0/255.0, alpha: 1.0)
    case .RJ:
      return UIColor(red: 0.0/255.0, green: 174.0/255.0, blue: 239.0/255.0, alpha: 1.0)
    case .RO:
      return UIColor(red: 158.0/255.0, green: 72.0/255.0, blue: 131.0/255.0, alpha: 1.0)
    case .RR:
      return UIColor(red: 146.0/255.0, green: 34.0/255.0, blue: 111.0/255.0, alpha: 1.0)
    case .SC:
      return UIColor(red: 175.0/255.0, green: 31.0/255.0, blue: 142.0/255.0, alpha: 1.0)
    case .SP:
      return UIColor(red: 236.0/255.0, green: 0.0/255.0, blue: 140.0/255.0, alpha: 1.0)
    case .SE:
      return UIColor(red: 237.0/255.0, green: 29.0/255.0, blue: 36.0/255.0, alpha: 1.0)
    case .TO:
      return UIColor(red: 161.0/255.0, green: 30.0/255.0, blue: 33.0/255.0, alpha: 1.0)
    default:
      return UIColor.blackColor()
    }
  }
  
  private func nameWithState(state: State) -> String {
    switch state {
    case .AC:
      return "Acre"
    case .AL:
      return "Alagoas"
    case .AP:
      return "Amapá"
    case .AM:
      return "Amazonas"
    case .BA:
      return "Bahia"
    case .CE:
      return "Ceará"
    case .DF:
      return "Distrito Federal"
    case .ES:
      return "Espírito Santo"
    case .GO:
      return "Goiás"
    case .MA:
      return "Maranhão"
    case .MS:
      return "Mato Grosso do Sul"
    case .MT:
      return "Mato Grosso"
    case .MG:
      return "Minas Gerais"
    case .PA:
      return "Pará"
    case .PB:
      return "Paraíba"
    case .PR:
      return "Paraná"
    case .PE:
      return "Pernambuco"
    case .PI:
      return "Piauí"
    case .RN:
      return "Rio Grande do Norte"
    case .RS:
      return "Rio Grande do Sul"
    case .RJ:
      return "Rio de Janeiro"
    case .RO:
      return "Rondônia"
    case .RR:
      return "Roraima"
    case .SC:
      return "Santa Catarina"
    case .SP:
      return "São Paulo"
    case .SE:
      return "Sergipe"
    case .TO:
      return "Tocantins"
    default:
      return ""
    }
  }
  
  private func mapPinImageWithState(state: State) -> String {
    switch state {
    case .AC:
      return "pin_acre"
    case .AL:
      return "pin_alagoas"
    case .AP:
      return "pin_amapa"
    case .AM:
      return "pin_amazonas"
    case .BA:
      return "pin_bahia"
    case .CE:
      return "pin_ceara"
    case .DF:
      return "pin_df"
    case .ES:
      return "pin_espirito_santo"
    case .GO:
      return "pin_goias"
    case .MA:
      return "pin_maranhao"
    case .MS:
      return "pin_mato_grosso_do_sul"
    case .MT:
      return "pin_mato_grosso"
    case .MG:
      return "pin_minas_gerais"
    case .PA:
      return "pin_para"
    case .PB:
      return "pin_paraiba"
    case .PR:
      return "pin_parana"
    case .PE:
      return "pin_pernambuco"
    case .PI:
      return "pin_piaui"
    case .RN:
      return "pin_rio_grande_do_norte"
    case .RS:
      return "pin_rio_grande_do_sul"
    case .RJ:
      return "pin_rj"
    case .RO:
      return "pin_rondonia"
    case .RR:
      return "pin_roraima"
    case .SC:
      return "pin_santa_catarina"
    case .SP:
      return "pin_sao_paulo"
    case .SE:
      return "pin_sergipe"
    case .TO:
      return "pin_tocantins"
    default:
      return ""
    }
  }
  
  private func pinImageWithState(state: State) -> String {
    switch state {
    case .AC:
      return "icn_pin_acre_detail"
    case .AL:
      return "icn_pin_alagoas_detail"
    case .AP:
      return "icn_pin_amapa_detail"
    case .AM:
      return "icn_pin_amazonas_detail"
    case .BA:
      return "icn_pin_bahia_detail"
    case .CE:
      return "icn_pin_ceara_detail"
    case .DF:
      return "icn_pin_df_detail"
    case .ES:
      return "icn_pin_espirito_santo_detail"
    case .GO:
      return "icn_pin_goias_detail"
    case .MA:
      return "icn_pin_maranhao_detail"
    case .MS:
      return "icn_pin_mato_grosso_do_sul_detail"
    case .MT:
      return "icn_pin_mato_grosso_detail"
    case .MG:
      return "icn_pin_minas_gerais_detail"
    case .PA:
      return "icn_pin_para_detail"
    case .PB:
      return "icn_pin_paraiba_detail"
    case .PR:
      return "icn_pin_parana_detail"
    case .PE:
      return "icn_pin_pernambuco_detail"
    case .PI:
      return "icn_pin_piaui_detail"
    case .RN:
      return "icn_pin_rio_grande_do_norte_detail"
    case .RS:
      return "icn_pin_rio_grande_do_sul_detail"
    case .RJ:
      return "icn_pin_rj_detail"
    case .RO:
      return "icn_pin_rondonia_detail"
    case .RR:
      return "icn_pin_roraima_detail"
    case .SC:
      return "icn_pin_santa_catarina_detail"
    case .SP:
      return "icn_pin_sp_detail"
    case .SE:
      return "icn_pin_sergipe_detail"
    case .TO:
      return "icn_pin_tocantins_detail"
    default:
      return ""
    }
  }
}
