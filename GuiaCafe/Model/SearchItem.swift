//
//  SearchItem.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 12/10/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class SearchItem: NSObject {
  var feature: Feature
  var mode: Bool
  
  init(feature: Feature, mode: Bool) {
    self.feature = feature
    self.mode = mode
    super.init()
  }
}
