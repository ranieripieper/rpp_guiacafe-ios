//
//  BubbleAnnotation.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 12/3/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit
import MapKit

class BubbleAnnotation: NSObject {
  var cafe: Cafe
  var coordinate: CLLocationCoordinate2D
  
  init(cafe: Cafe, coordinate: CLLocationCoordinate2D) {
    self.cafe = cafe
    self.coordinate = coordinate
    super.init()
  }
}

extension BubbleAnnotation: MKAnnotation {
  var title: String { return cafe.name }
}
