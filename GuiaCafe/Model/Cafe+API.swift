//
//  Cafe+API.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 1/13/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit
import MapKit

extension Cafe {
  // MARK: Public
  class func getAllCafesWithUserLocation(userLocation: CLLocation, page: Int, perPage: Int, completion: [Cafe] -> ()) {
    getCafesWithRouter(.Location(userLocation.coordinate.latitude, userLocation.coordinate.longitude, page, perPage)) { cafes in
      completion(cafes ?? [])
    }
  }
  
  class func getAllCafesWithRatingAtPage(page: Int, perPage: Int, completion: [Cafe] -> ()) {
    getCafesWithRouter(.Rating(page, perPage)) { cafes in
      completion(cafes ?? [])
    }
  }
  
  class func getCafesWithTerm(term: String?, features: [Feature]?, page: Int, perPage: Int, completion: (cafes: [Cafe]?) -> ()) {
    let feats = features?.map() { feature -> String in
      feature.type
    }
    getCafesWithRouter(.Search(term ?? "", feats ?? [], page, perPage), completion: completion)
  }
  
  class func getTop20Cafes(page: Int, perPage: Int, completion: (cafes: [Cafe]?) -> ()) {
    getCafesWithRouter(.Top20(page, perPage), completion: completion)
  }
  
  class func getNewcomersCafes(page: Int, perPage: Int, completion: (cafes: [Cafe]?) -> ()) {
    getCafesWithRouter(.Revelation(page, perPage), completion: completion)
  }
  
  class func getFavoritesCafes(page: Int, perPage: Int, completion: (cafes: [Cafe]?) -> ()) {
    getCafesWithRouter(.GetFavorites(page, perPage), completion: completion)
  }
  
  func favorite(favorite: Bool, completion: (Bool) -> ()) {
    let token = SSKeychain.passwordForService("guiacafeterias", account: "guiacafeterias")
    var params: [String: AnyObject]?
    if token != nil && count(token) > 0 {
      params = ["accessToken": token!]
    }
    Manager.sharedInstance.request(.POST, Router.Favorite(favorite, uuid), parameters: params, encoding: .URL)
    .responseJSON(completionHandler: { request, response, data, error in
      if let responseData = data as? [String: AnyObject], let success = responseData["success"] as? Bool {
        completion(success)
        return
      }
      completion(false)
    })
  }
  
  func rate(rating: Int, completion: (Bool) -> ()) {
    let token = SSKeychain.passwordForService("guiacafeterias", account: "guiacafeterias")
    var params: [String: AnyObject] = ["rating": rating]
    if token != nil && count(token) > 0 {
      params["accessToken"] = token!
    }
    Manager.sharedInstance.request(.POST, Router.Rate(uuid), parameters: params, encoding: .URL)
    .responseJSON(completionHandler: { request, response, data, error in
      completion(error == nil)
    })
  }
  
  func refreshCafeDetails(completion: () -> ()) {
    let token = SSKeychain.passwordForService("guiacafeterias", account: "guiacafeterias")
    var params = [String: AnyObject]()
    if token != nil && count(token) > 0 {
      params["accessToken"] = token!
    }
    Manager.sharedInstance.request(.GET, Router.Cafe(uuid), parameters: params, encoding: .URL)
      .responseJSON(completionHandler: { request, response, data, error in
        if let responseData = data as? [String: AnyObject], let dataArray = responseData["data"] as? [[String: AnyObject]], let dataDict = dataArray.first, let ratingsDict = dataDict["ratings"] as? [String: Float] {
          self.rating = ratingsDict["average"]!
          self.ratingCount = Int(ratingsDict["total"]!)
        }
        completion()
      })
  }
  
  // MARK: Private
  private class func getCafesWithRouter(router: Router, completion: (cafes: [Cafe]?) -> ()) {
    let token = SSKeychain.passwordForService("guiacafeterias", account: "guiacafeterias")
    var params: [String: AnyObject]?
    if token != nil && count(token) > 0 {
      params = ["accessToken": token!]
    }
    let urlString = router.URLString.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)
    Manager.sharedInstance.request(.GET, urlString!, parameters: params, encoding: .URL)
      .response { request, response, data, error in
        if error == nil {
          dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            let cafes = self.handleResponseData(data)
            dispatch_async(dispatch_get_main_queue()) {
              completion(cafes: cafes)
            }
          }
        } else {
          completion(cafes: nil)
        }
    }
  }
  
  private class func handleResponseData(data: AnyObject?) -> [Cafe] {
    let json = JSON(data: data as! NSData)
    if json["success"].bool != nil && json["success"].bool! {
      if let cafesArray = json["data"].array {
        var cafes = [Cafe]()
        for cafeJSON in cafesArray {
          let cafe = Cafe.cafeWithAPIJSON(cafeJSON)!
          cafes.append(cafe)
        }
        return cafes
      } else {
        return []
      }
    } else {
      return []
    }
  }

  private class func cafeWithAPIJSON(json: JSON) -> Cafe? {
    let geolocation = json["address"]["geolocation"].dictionary
    let contact = json["contact"].dictionary
    let premises = json["establishment"].dictionary
    let featuresDict = premises!["itemsHouse"]!.dictionary
    let featuresDict2 = json["coffee"].dictionary
    let information = json["informations"].dictionary
    let photosArray = json["photos"].array
    let ratingsDict = json["ratings"].dictionary
    let userDataDict = json["userData"].dictionary
    
    let name = json["name"].string ?? ""
    let uuid = json["uuid"].string ?? ""
    let slug = json["slug"].string ?? ""
    let address = json["formatedAddress"].string ?? ""
    let state = json["address"]["state"].string ?? ""
    let franchise = json["location"].string ?? ""
    let latitude = geolocation?["latitude"]?.number ?? 0
    let longitude = geolocation?["longitude"]?.number ?? 0
    let location = CLLocation(latitude: latitude as Double, longitude: longitude as Double)
    let phone = contact?["phone"]?.string ?? ""
    let website = contact?["website"]?.string ?? ""
    
    let photos = photosArray?.map() { photo -> String in
      photo["url"].string ?? ""
    }
    
    let establishment = premises?["opening"]?.string ?? ""
    var features = [Feature]()
    var facilities = [Feature]()
    for (key, value) in featuresDict! {
      if value.bool == true || value.number == 1 || value.string == "1" {
        let feature = Feature(type: key)
        switch Feature.featureTypeWithString(key) {
          case .Feature:
            features.append(feature)
          case .Facility:
            facilities.append(feature)
          default:
            break
        }
      }
    }
    for (key, value) in featuresDict2! {
      if value.bool == true || value.number == 1 || value.string == "1" {
        let feature = Feature(type: key)
        switch Feature.featureTypeWithString(key) {
        case .Feature:
          features.append(feature)
        case .Facility:
          facilities.append(feature)
        default:
          break
        }
      }
    }
    if (premises?["courses"]?.bool ?? premises?["courses"]?.number == 1) || premises?["courses"]?.string == "1" {
      features.append(Feature(type: "courses"))
    }
    if (premises?["liveMusic"]?.bool ?? premises?["liveMusic"]?.number == 1) || premises?["liveMusic"]?.string == "1" {
      features.append(Feature(type: "liveMusic"))
    }
    if (premises?["closeTouristPoint"]?.bool ?? premises?["closeTouristPoint"]?.number == 1) || premises?["closeTouristPoint"]?.string == "1" {
      features.append(Feature(type: "closeTouristPoint"))
    }
    if (premises?["closeMetroPoint"]?.bool ?? premises?["closeMetroPoint"]?.number == 1) || premises?["closeMetroPoint"]?.string == "1" {
      features.append(Feature(type: "closeMetroPoint"))
    }
    if (featuresDict2?["alternateTypes"]?["decaffeinated"].bool ?? featuresDict2?["alternateTypes"]?["decaffeinated"].number == 1) || featuresDict2?["alternateTypes"]?["decaffeinated"].string == "1" {
      features.append(Feature(type: "decaffeinated"))
    }
    if (json["payments"]["check"].bool ?? json["payments"]["check"].number == 1) || json["payments"]["check"].string == "1" {
      facilities.append(Feature(type: "check"))
    }
    
    let sitting = premises?["capacityHome"]?.string ?? ""
    
    let review = json["resume"].string ?? ""
    
    let service = information?["serve"]?.string ?? ""
    let extracted = information?["extraction"]?.string ?? ""
    let drink = information?["toDrink"]?.string ?? ""
    let eat = information?["toEat"]?.string ?? ""
    let strongs = information?["strongPoints"]?.string ?? ""

    let top20 = json["isTop20"].bool ?? false
    let newcomer = json["isCafeteriaRevelacao"].bool ?? false
    let premium = json["isFeatured"].bool ?? false
    
    let cards = json["formatedPaymentsBrands"].string ?? ""
    let tickets = json["formatedPaymentsTypeTickets"].string ?? ""
    
    let hours = json["formatedHours"].string ?? ""
    
    let ratingAverage = ratingsDict?["average"]?.number ?? 0
    let ratingTotal = ratingsDict?["total"]?.number ?? 0
    
    let favorite = userDataDict?["favorite"]?.bool ?? false
    
    let swiftRange = hours.startIndex..<advance(hours.startIndex, count(hours))
    let workingHours = hours.stringByReplacingOccurrencesOfString("; ", withString: ";\n", options: .CaseInsensitiveSearch, range: swiftRange)

    let cafe = Cafe(name: name, uuid: uuid, franchise: franchise, location: location, contact: Contact(email: "", phone: phone, website: website), address: address, state: State(string: state), images: photos ?? [], establishment: establishment, rating: ratingAverage.floatValue, ratingCount: ratingTotal.integerValue, service: service, favorited: favorite, top20: top20, newcomer: newcomer, premium: premium, extracted: extracted, drink: drink, eat: eat, strongs: strongs, features: features, review: review, workingHour: workingHours, sitting: sitting, cards: cards, tickets: tickets, facilities: facilities)
    return cafe
  }
}
