//
//  Contact.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 1/16/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class Contact: NSObject {
  var email: String?
  var phone: String?
  var website: String?
  
  init(email: String?, phone: String?, website: String?) {
    self.email = email
    self.phone = phone
    self.website = website
    super.init()
  }
}
