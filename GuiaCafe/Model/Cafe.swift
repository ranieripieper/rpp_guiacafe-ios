//
//  Cafe.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 12/1/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit
import MapKit

class Cafe: NSObject {
  var name: String
  var uuid: String
  var franchise: String
  var location: CLLocation
  var address: String
  var state: State
  var contact: Contact
  var images: [String]
  var establishment: String
  var rating: Float
  var ratingCount: Int
  var favorited: Bool
  var top20: Bool
  var newcomer: Bool
  var premium: Bool
  var service: String
  var extracted: String
  var drink: String
  var eat: String
  var strongs: String
  var features: [Feature]
  var review: String
  var workingHour: String
  var sitting: String
  var cards: String
  var tickets: String
  var facilities: [Feature]
  
  init(name: String, uuid: String, franchise: String, location: CLLocation, contact: Contact, address: String, state: State, images: [String], establishment: String, rating: Float, ratingCount: Int, service: String, favorited: Bool, top20: Bool, newcomer: Bool, premium: Bool, extracted: String, drink: String, eat: String, strongs: String, features: [Feature], review: String, workingHour: String, sitting: String, cards: String, tickets: String, facilities: [Feature]) {
    self.name = name
    self.uuid = uuid
    self.franchise = franchise
    self.location = location
    self.contact = contact
    self.address = address
    self.state = state
    self.images = images
    self.establishment = establishment
    self.rating = rating
    self.ratingCount = ratingCount
    self.service = service
    self.favorited = favorited
    self.top20 = top20
    self.newcomer = newcomer
    self.premium = premium
    self.extracted = extracted
    self.drink = drink
    self.eat = eat
    self.strongs = strongs
    self.features = features
    self.review = review
    self.workingHour = workingHour
    self.sitting = sitting
    self.cards = cards
    self.tickets = tickets
    self.facilities = facilities
    super.init()
  }
  
  func details() -> [String] {
    var details = [String]()
    if images.count > 0 {
      details.append("IMAGE")
    }
    if count(name) > 0 {
      details.append("INFO")
    }
    if count(service) > 0 {
      details.append("SERVE")
    }
    if count(extracted) > 0 {
      details.append("EXTRAÍDO DE UMA")
    }
    if count(drink) > 0 {
      details.append("PARA BEBER")
    }
    if count(eat) > 0 {
      details.append("PARA COMER")
    }
    if count(strongs) > 0 {
      details.append("PONTO FORTE")
    }
    if features.count > 0 {
      details.append("FEATURES")
    }
    if count(workingHour) > 0 || count(sitting) > 0 || count(cards) > 0 || count(tickets) > 0 {
      details.append("SERVIÇO")
    }
    if count(review) > 0 {
      details.append("Sobre a Cafeteria")
    }
    details.append("SHARE")
    return details
  }
}

extension Cafe: MKAnnotation {
  var title: String { return name }
  var coordinate: CLLocationCoordinate2D { return location.coordinate }
}
