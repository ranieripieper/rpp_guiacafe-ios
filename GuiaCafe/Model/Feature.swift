//
//  Feature.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 12/10/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

enum FeatureType {
  case None
  case Feature
  case Facility
}

class Feature: NSObject {

  let name: String
  let icon: String
  let type: String
  let searchIcon: String
  let order: Int
  
  init(type: String) {
    self.name = Feature.stringWithType(type)
    self.icon = Feature.iconWithType(type)
    self.searchIcon = "\(icon)_search"
    self.type = type
    self.order = Feature.order(type)
    super.init()
  }
  
  func toString() -> String {
    switch self.name {
    case "WIFI":
      return "TEM WI-FI"
    case "ATTESTED":
      return "TEM BARISTA FORMADO OU CERFITICADO"
    case "TASTING":
      return "OFERECE DEGUSTAÇÃO DE CAFÉ"
    case "ROASTING":
      return "TORRA O CAFÉ"
    case "TNT":
      return "REALIZA O CAMPEONATO TNT"
    case "CLOSETOURISTPOINT":
      return "É PRÓXIMO A PONTO TURÍSTICO"
    case "CLOSEMETROPOINT":
      return "É PRÓXIMO A ESTAÇÃO DE METRÔ OU TREM"
    case "LIVEMUSIC":
      return "TEM MÚSICA AO VIVO"
    case "OPENAIR":
      return "TEM ÁREA AO AR LIVRE"
    case "DECAFFEINATED":
      return "OFERECE CAFÉ DESCAFEINADO"
    case "HASALTERNATETYPES":
      return "OFERECE OPÇÕES PARA QUEM TEM RESTRIÇÕES ALIMENTARES"
    case "COURSES":
      return "OFERECE CURSOS"
    case "HASAWARDS":
      return "O CAFÉ E/OU A CAFETERIA TÊM PRÊMIOS OU CERTIFICAÇÕES"
    case "TOGOBUY":
      return "VENDE CAFÉ PARA SAIR TOMANDO"
    case "TOBUY":
      return "VENDE CAFÉ EM GRÃOS OU MOÍDO E/OU XÍCARAS E ACESSÓRIOS"
      
    case "CHECK":
      return "ACEITA CHEQUE"
    case "ACCESSIBILITY":
      return "TEM ACESSO PARA DEFICIENTES"
    case "COMPRESSEDAIR":
      return "TEM AR CONDICIONADO"
    case "WC":
      return "TEM BANHEIROS"
    case "SMOKING":
      return "COM ÁREA PARA FUMANTES"
    case "PARKING":
      return "TEM ESTACIONAMENTO"
    default:
      return ""
    }
  }

  class func featureTypeWithString(string: String) -> FeatureType {
    switch string.uppercaseString {
    case "WIFI", "ATTESTED", "TASTING", "ROASTING", "TNT", "CLOSETOURISTPOINT", "CLOSEMETROPOINT", "LIVEMUSIC", "OPENAIR", "DECAFFEINATED", "HASALTERNATETYPES", "COURSES", "HASAWARDS", "TOGOBUY", "TOBUY":
      return .Feature
    case "CHECK", "ACCESSIBILITY", "COMPRESSEDAIR", "WC", "SMOKING", "PARKING":
      return .Facility
    default:
      return .None
    }
  }

  class func stringWithType(type: String) -> String {
    switch type.uppercaseString {
    case "WIFI":
      return "TEM WI-FI"
    case "ATTESTED":
      return "TEM BARISTA FORMADO OU CERFITICADO"
    case "TASTING":
      return "OFERECE DEGUSTAÇÃO DE CAFÉ"
    case "ROASTING":
      return "TORRA O CAFÉ"
    case "TNT":
      return "REALIZA O CAMPEONATO TNT"
    case "CLOSETOURISTPOINT":
      return "É PRÓXIMO A PONTO TURÍSTICO"
    case "CLOSEMETROPOINT":
      return "É PRÓXIMO A ESTAÇÃO DE METRÔ OU TREM"
    case "LIVEMUSIC":
      return "TEM MÚSICA AO VIVO"
    case "OPENAIR":
      return "TEM ÁREA AO AR LIVRE"
    case "DECAFFEINATED":
      return "OFERECE CAFÉ DESCAFEINADO"
    case "HASALTERNATETYPES":
      return "OFERECE OPÇÕES PARA QUEM TEM RESTRIÇÕES ALIMENTARES"
    case "COURSES":
      return "OFERECE CURSOS"
    case "HASAWARDS":
      return "O CAFÉ E/OU A CAFETERIA TÊM PRÊMIOS OU CERTIFICAÇÕES"
    case "TOGOBUY":
      return "VENDE CAFÉ PARA SAIR TOMANDO"
    case "TOBUY":
      return "VENDE CAFÉ EM GRÃOS OU MOÍDO E/OU XÍCARAS E ACESSÓRIOS"

    case "CHECK":
      return "ACEITA CHEQUE"
    case "ACCESSIBILITY":
      return "TEM ACESSO PARA DEFICIENTES"
    case "COMPRESSEDAIR":
      return "TEM AR CONDICIONADO"
    case "WC":
      return "TEM BANHEIROS"
    case "SMOKING":
      return "COM ÁREA PARA FUMANTES"
    case "PARKING":
      return "TEM ESTACIONAMENTO"
    default:
      return ""
    }
  }
  
  class func iconWithType(type: String) -> String {
    switch type.uppercaseString {
    case "WIFI":
      return "icn_wifi"
    case "ATTESTED":
      return "icn_certificado"
    case "TASTING":
      return "icn_degustação"
    case "ROASTING":
      return "icn_torra_cafe"
    case "TNT":
      return "icn_tnt"
    case "CLOSETOURISTPOINT":
      return "icn_turistico"
    case "CLOSEMETROPOINT":
      return "icn_trem"
    case "LIVEMUSIC":
      return "icn_musica"
    case "OPENAIR":
      return "icn_area_ar_livre"
    case "DECAFFEINATED":
      return "icn_decafeinado"
    case "HASALTERNATETYPES":
      return "icn_restrições"
    case "COURSES":
      return "icn_cursos"
    case "HASAWARDS":
      return "icn_premiado"
    case "TOGOBUY":
      return "icn_para_levar"
    case "TOBUY":
      return "icn_presente"
      
    case "CHECK":
      return "icn_cheque"
    case "ACCESSIBILITY":
      return "icn_acesso"
    case "COMPRESSEDAIR":
      return "icn_ar_condicionado"
    case "WC":
      return "icn_banheiros"
    case "SMOKING":
      return "icn_area_para_fumantes"
    case "PARKING":
      return "icn_estacionamento"
    default:
      return ""
    }
  }
  
  class func order(type: String) -> Int {
    switch type.uppercaseString {
    case "WIFI":
      return 1
    case "ATTESTED":
      return 2
    case "TASTING":
      return 3
    case "ROASTING":
      return 4
    case "TNT":
      return 5
    case "CLOSETOURISTPOINT":
      return 6
    case "CLOSEMETROPOINT":
      return 7
    case "LIVEMUSIC":
      return 8
    case "OPENAIR":
      return 9
    case "DECAFFEINATED":
      return 10
    case "HASALTERNATETYPES":
      return 11
    case "COURSES":
      return 12
    case "HASAWARDS":
      return 13
    case "TOGOBUY":
      return 14
    case "TOBUY":
      return 15
      
    case "CHECK":
      return 16
    case "ACCESSIBILITY":
      return 17
    case "COMPRESSEDAIR":
      return 18
    case "WC":
      return 19
    case "SMOKING":
      return 20
    case "PARKING":
      return 21
    default:
      return 100
    }
  }
}
