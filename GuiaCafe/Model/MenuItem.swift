//
//  MenuItem.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 12/10/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import Foundation

enum MenuItem {
  case CafeList
  case Search
  case Top20
  case Newcomers
  case Favorites
  case About
  case Login
}

func textForItem(menuItem: MenuItem) -> String {
  switch menuItem {
  case .CafeList:
    return "Todas as Cafeterias"
  case .Search:
    return "Busca de Cafeteria"
  case .Top20:
    return "Cafeterias Top 20"
  case .Newcomers:
    return "Cafeterias Revelação"
  case .Favorites:
    return "Cafeterias Favoritas"
  case .About:
    return "Sobre"
  case .Login:
    return "Login"
  }
}
