//
//  AppDelegate.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 12/1/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  var window: UIWindow?

  func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
    if !Session.isLoggedIn() {
      SSKeychain.deletePasswordForService("guiacafeterias", account: "guiacafeterias")
    }
    return true
  }
  
  func resumeMainApplicationFlow() {
    window!.rootViewController = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle()).instantiateViewControllerWithIdentifier("MainNavigationController") as! MainNavigationController
  }
  
  func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject?) -> Bool {
    if url.absoluteString!.hasPrefix("fb1580831572154286") {
      return FBSession.activeSession().handleOpenURL(url)
    } else {
      return GPPURLHandler.handleURL(url, sourceApplication: sourceApplication, annotation: annotation)
    }
  }
}
