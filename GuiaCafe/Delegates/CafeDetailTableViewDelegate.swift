//
//  CafeDetailTableViewDelegate.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 12/11/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit
import MapKit

@objc protocol CafeDetailTableViewDelegateDelegate {
  func call()
  func goToCafe()
  func goToWebsite()
}

class CafeDetailTableViewDelegate: NSObject {
  @IBOutlet weak var cafeDetailViewController: CafeDetailViewController!
  
  weak var delegate: CafeDetailTableViewDelegateDelegate?
  var onceToken: dispatch_once_t = 0
  var sizingCell: CafeDetailDetailCell?
  var serviceOnceToken: dispatch_once_t = 0
  var serviceSizingCell: CafeDetailServiceCell?
  var imageCellContentOffset: CGPoint?
}

extension CafeDetailTableViewDelegate: UITableViewDataSource, UITableViewDelegate {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return cafeDetailViewController.cafe!.details().count
  }
  
  func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let header = HeaderView(frame: CGRect(x: 0, y: 0, width: cafeDetailViewController.view.bounds.width, height: 30))
    header.configureWithState(cafeDetailViewController.cafe!.state)
    return header
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let details = cafeDetailViewController.cafe!.details()
    switch details[indexPath.row] {
    case "IMAGE":
      let imageCell = tableView.dequeueReusableCellWithIdentifier("CafeDetailImageCell", forIndexPath: indexPath) as! CafeDetailImageCell
      imageCell.delegate = self
      imageCell.configureWithImages(cafeDetailViewController.cafe!.images, initialContentOffset: imageCellContentOffset)
      return imageCell
    case "INFO":
      let infoCell = tableView.dequeueReusableCellWithIdentifier("CafeDetailInfoCell", forIndexPath: indexPath) as! CafeDetailInfoCell
      var locationManager: CLLocationManager
      if let mainNavigationController = cafeDetailViewController.navigationController as? MainNavigationController {
        locationManager = mainNavigationController.locationManager
      } else {
        locationManager = cafeDetailViewController.mainNavigationController!.locationManager
      }
      if locationManager.location != nil {
        let distance = (cafeDetailViewController.navigationController as! MainNavigationController).locationManager.location.distanceFromLocation(cafeDetailViewController.cafe!.location) / 1000
        infoCell.configureWithCafe(cafeDetailViewController.cafe!, distance: distance)
      } else {
        infoCell.configureWithCafe(cafeDetailViewController.cafe!, distance: nil)
      }
      infoCell.cafeDetailViewController = cafeDetailViewController
      return infoCell
    case "SERVE":
      let detailCell = tableView.dequeueReusableCellWithIdentifier("CafeDetailDetailCell", forIndexPath: indexPath) as! CafeDetailDetailCell
      detailCell.configureWithTitle("SERVE", titleColor: cafeDetailViewController.cafe!.state.info().1, description: cafeDetailViewController.cafe!.service)
      return detailCell
    case "EXTRAÍDO DE UMA":
      let detailCell = tableView.dequeueReusableCellWithIdentifier("CafeDetailDetailCell", forIndexPath: indexPath) as! CafeDetailDetailCell
      detailCell.configureWithTitle("EXTRAÍDO DE UMA", titleColor: cafeDetailViewController.cafe!.state.info().1, description: cafeDetailViewController.cafe!.extracted)
      return detailCell
    case "PARA BEBER":
      let detailCell = tableView.dequeueReusableCellWithIdentifier("CafeDetailDetailCell", forIndexPath: indexPath) as! CafeDetailDetailCell
      detailCell.configureWithTitle("PARA BEBER", titleColor: cafeDetailViewController.cafe!.state.info().1, description: cafeDetailViewController.cafe!.drink)
      return detailCell
    case "PARA COMER":
      let detailCell = tableView.dequeueReusableCellWithIdentifier("CafeDetailDetailCell", forIndexPath: indexPath) as! CafeDetailDetailCell
      detailCell.configureWithTitle("PARA COMER", titleColor: cafeDetailViewController.cafe!.state.info().1, description: cafeDetailViewController.cafe!.eat)
      return detailCell
    case "PONTO FORTE":
      let detailCell = tableView.dequeueReusableCellWithIdentifier("CafeDetailDetailCell", forIndexPath: indexPath) as! CafeDetailDetailCell
      detailCell.configureWithTitle("PONTO FORTE", titleColor: cafeDetailViewController.cafe!.state.info().1, description: cafeDetailViewController.cafe!.strongs)
      return detailCell
    case "FEATURES":
      let featuresCell = tableView.dequeueReusableCellWithIdentifier("CafeDetailFeaturesCell", forIndexPath: indexPath) as! CafeDetailFeaturesCell
      featuresCell.configureWithFeatures(cafeDetailViewController.cafe!.features)
      featuresCell.delegate = self
      return featuresCell
    case "SERVIÇO":
      let serviceCell = tableView.dequeueReusableCellWithIdentifier("CafeDetailServiceCell", forIndexPath: indexPath) as! CafeDetailServiceCell
      let cafe = cafeDetailViewController.cafe!
      serviceCell.configureWithColor(cafe.state.info().1, address: cafe.address, workingHours: cafe.workingHour, phone: cafe.contact.phone!, sitting: cafe.sitting, website: cafe.contact.website!, cards: cafe.cards, tickets: cafe.tickets, facilities: cafe.facilities)
      serviceCell.delegate = self
      return serviceCell
    case "Sobre a Cafeteria":
      let detailCell = tableView.dequeueReusableCellWithIdentifier("CafeDetailDetailCell", forIndexPath: indexPath) as! CafeDetailDetailCell
      detailCell.configureWithTitle("Sobre a Cafeteria", titleColor: cafeDetailViewController.cafe!.state.info().1, description: cafeDetailViewController.cafe!.review)
      detailCell.addTopLine()
      detailCell.setNeedsUpdateConstraints()
      detailCell.updateConstraintsIfNeeded()
      return detailCell
    case "SHARE":
      let shareCell = tableView.dequeueReusableCellWithIdentifier("CafeDetailShareCell", forIndexPath: indexPath) as! CafeDetailShareCell
      shareCell.cafeDetailViewController = cafeDetailViewController
      return shareCell
    default:
      return UITableViewCell()
    }
  }
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return heightForCellInTableView(tableView, indexPath: indexPath)
  }
  
  func heightForCellInTableView(tableView: UITableView, indexPath: NSIndexPath) -> CGFloat {
    switch cafeDetailViewController.cafe!.details()[indexPath.row] {
    case "IMAGE":
      return 194
    case "INFO":
      let hasBadges = cafeDetailViewController.cafe!.top20 || cafeDetailViewController.cafe!.premium || cafeDetailViewController.cafe!.newcomer
      return (count(cafeDetailViewController.cafe!.franchise) > 0 ? 170.0 : 150.0) - (hasBadges ? 0.0 : 54.0)
    case "SERVE":
      dispatch_once(&onceToken) {
        self.sizingCell = tableView.dequeueReusableCellWithIdentifier("CafeDetailDetailCell") as? CafeDetailDetailCell
      }
      sizingCell!.configureWithTitle(cafeDetailViewController.cafe!.details()[indexPath.row], titleColor: cafeDetailViewController.cafe!.state.info().1, description: cafeDetailViewController.cafe!.service)
      let height = calculateHeightForConfiguredCell(sizingCell!)
      return height
    case "EXTRAÍDO DE UMA":
      dispatch_once(&onceToken) {
        self.sizingCell = tableView.dequeueReusableCellWithIdentifier("CafeDetailDetailCell") as? CafeDetailDetailCell
      }
      sizingCell!.configureWithTitle(cafeDetailViewController.cafe!.details()[indexPath.row], titleColor: cafeDetailViewController.cafe!.state.info().1, description: cafeDetailViewController.cafe!.extracted)
      let height = calculateHeightForConfiguredCell(sizingCell!)
      return height
    case "PARA BEBER":
      dispatch_once(&onceToken) {
        self.sizingCell = tableView.dequeueReusableCellWithIdentifier("CafeDetailDetailCell") as? CafeDetailDetailCell
      }
      sizingCell!.configureWithTitle(cafeDetailViewController.cafe!.details()[indexPath.row], titleColor: cafeDetailViewController.cafe!.state.info().1, description: cafeDetailViewController.cafe!.drink)
      let height = calculateHeightForConfiguredCell(sizingCell!)
      return height
    case "PARA COMER":
      dispatch_once(&onceToken) {
        self.sizingCell = tableView.dequeueReusableCellWithIdentifier("CafeDetailDetailCell") as? CafeDetailDetailCell
      }
      sizingCell!.configureWithTitle(cafeDetailViewController.cafe!.details()[indexPath.row], titleColor: cafeDetailViewController.cafe!.state.info().1, description: cafeDetailViewController.cafe!.eat)
      let height = calculateHeightForConfiguredCell(sizingCell!)
      return height
    case "PONTO FORTE":
      dispatch_once(&onceToken) {
        self.sizingCell = tableView.dequeueReusableCellWithIdentifier("CafeDetailDetailCell") as? CafeDetailDetailCell
      }
      sizingCell!.configureWithTitle(cafeDetailViewController.cafe!.details()[indexPath.row], titleColor: cafeDetailViewController.cafe!.state.info().1, description: cafeDetailViewController.cafe!.strongs)
      let height = calculateHeightForConfiguredCell(sizingCell!)
      return height
    case "FEATURES":
      let height = CGFloat(Int((CGFloat(cafeDetailViewController.cafe!.features.count) - 0.1) / CGFloat(tableView.bounds.width / 40.0) + 1)) * 40.0 + 40.0
      return height
    case "SERVIÇO":
      dispatch_once(&serviceOnceToken) {
        self.serviceSizingCell = tableView.dequeueReusableCellWithIdentifier("CafeDetailServiceCell") as? CafeDetailServiceCell
      }
      let cafe = cafeDetailViewController.cafe!
      serviceSizingCell!.configureWithColor(cafe.state.info().1, address: cafe.address, workingHours: cafe.workingHour, phone: cafe.contact.phone!, sitting: cafe.sitting, website: cafe.contact.website!, cards: cafe.cards, tickets: cafe.tickets, facilities: cafe.facilities)
      let height = calculateHeightForConfiguredCell(serviceSizingCell!)
      return height
    case "Sobre a Cafeteria":
      dispatch_once(&onceToken) {
        self.sizingCell = tableView.dequeueReusableCellWithIdentifier("CafeDetailDetailCell") as? CafeDetailDetailCell
      }
      sizingCell!.configureWithTitle(cafeDetailViewController.cafe!.details()[indexPath.row], titleColor: cafeDetailViewController.cafe!.state.info().1, description: cafeDetailViewController.cafe!.review)
      let height = calculateHeightForConfiguredCell(sizingCell!)
      return height
    default:
      return 50
    }
  }
  
  func calculateHeightForConfiguredCell(cell: UITableViewCell) -> CGFloat {
    cell.setNeedsUpdateConstraints()
    cell.updateConstraintsIfNeeded()
    cell.bounds = CGRect(x: 0.0, y: 0.0, width: UIScreen.mainScreen().bounds.width, height: cell.bounds.height)
    cell.setNeedsLayout()
    cell.layoutIfNeeded()
    let size = cell.contentView.systemLayoutSizeFittingSize(UILayoutFittingCompressedSize)
    return size.height + 1.0
  }
}

extension CafeDetailTableViewDelegate: UIScrollViewDelegate {
  func scrollViewDidScroll(scrollView: UIScrollView) {
    if let cell = cafeDetailViewController.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0)) as? CafeDetailImageCell {
      let ratio: CGFloat = 0.6
      cell.scrollView.contentOffset = CGPoint(x: cell.scrollView.contentOffset.x, y: min(-ratio * (scrollView.contentOffset.y + 64.0), 0))
    }
  }
}

extension CafeDetailTableViewDelegate: CafeDetailFeaturesCellDelegate {
  func didSelectFeature(feature: Feature) {
    cafeDetailViewController.performSegueWithIdentifier("SegueFeatures", sender: cafeDetailViewController.cafe!.features)
  }
}

extension CafeDetailTableViewDelegate: CafeDetailServiceCellDelegate {
  func didSelectFacility(facility: Feature) {
    cafeDetailViewController.performSegueWithIdentifier("SegueFeatures", sender: cafeDetailViewController.cafe!.facilities)
  }
  
  func didTapAddress() {
    delegate?.goToCafe()
  }
  
  func didTapPhone() {
    delegate?.call()
  }
  
  func didTapWebsite() {
    delegate?.goToWebsite()
  }
}

extension CafeDetailTableViewDelegate: CafeDetailImageCellDelegate {
  func didScroll(scrollView: UIScrollView) {
    imageCellContentOffset = scrollView.contentOffset
  }
}
