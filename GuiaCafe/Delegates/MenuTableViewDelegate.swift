//
//  MenuTableViewDelegate.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 12/10/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

@objc protocol MenuTableViewDelegateDelegate {
  func goToCafeList()
  func goToSearch()
  func goToTop20()
  func goToNewcomers()
  func goToFavorites()
  func goToAbout()
  func goToLogin()
}

class MenuTableViewDelegate: NSObject {
  weak var delegate: MenuTableViewDelegateDelegate?
  let menuItems = [MenuItem.CafeList, MenuItem.Search, MenuItem.Top20, MenuItem.Newcomers, MenuItem.Favorites, MenuItem.About, MenuItem.Login]
}

extension MenuTableViewDelegate: UITableViewDataSource, UITableViewDelegate {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return menuItems.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let menuCell = tableView.dequeueReusableCellWithIdentifier("MenuCell", forIndexPath: indexPath) as! MenuCell
    menuCell.configureWithMenuItem(menuItems[indexPath.row])
    return menuCell
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    tableView.deselectRowAtIndexPath(indexPath, animated: true)
    if indexPath.row == 0 {
      delegate?.goToCafeList()
    } else if indexPath.row == 1 {
      delegate?.goToSearch()
    } else if indexPath.row == 2 {
      delegate?.goToTop20()
    } else if indexPath.row == 3 {
      delegate?.goToNewcomers()
    } else if indexPath.row == 4 {
      delegate?.goToFavorites()
    } else if indexPath.row == 5 {
      delegate?.goToAbout()
    } else {
      delegate?.goToLogin()
    }
  }
  
  func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    return UIView()
  }
}
