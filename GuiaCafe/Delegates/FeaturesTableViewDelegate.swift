//
//  FeaturesTableViewDelegate.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 12/11/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class FeaturesTableViewDelegate: NSObject {
  var features: [Feature]?
}

extension FeaturesTableViewDelegate: UITableViewDataSource, UITableViewDelegate {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return features?.count ?? 0
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let featuresCell = tableView.dequeueReusableCellWithIdentifier("FeaturesCell", forIndexPath: indexPath) as! FeaturesCell
    featuresCell.configureWithFeature(features![indexPath.row])
    return featuresCell
  }
}
