//
//  CafeListTableViewDelegate.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 12/10/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit
import MapKit

@objc protocol CafeListTableViewDelegateDelegate {
  func tableViewDidScroll(tableView: UITableView)
  func tableViewDidStop(tableView: UITableView)
}

class CafeListTableViewDelegate: NSObject {
  @IBOutlet weak var cafeListViewController: CafeListViewController!
  
  weak var delegate: CafeListTableViewDelegateDelegate?
  var datasource: [Cafe] = []
  let imageCache = NSCache()
}

extension CafeListTableViewDelegate: UITableViewDataSource, UITableViewDelegate {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return datasource.count
  }
  
  func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return cafeListViewController.type == .Search || section > 0 ? 0 : 30
  }
  
  func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    if cafeListViewController.type == .Search || section > 0 {
      return nil
    }
    let headerView = HeaderView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 30))
    switch cafeListViewController.type {
    case .All:
      headerView.configureWithString("Cafeterias")
    case .Top20:
      headerView.configureWithString("Top 20")
    case .Newcomers:
      headerView.configureWithString("Cafeterias Revelação")
    case .Favorites:
      headerView.configureWithString("Favoritos")
    default:
      return nil
    }
    return headerView
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cafeCell = tableView.dequeueReusableCellWithIdentifier("CafeCell", forIndexPath: indexPath) as! CafeCell
    let cafe = datasource[indexPath.row]
    let locationManager = (cafeListViewController.navigationController as! MainNavigationController).locationManager
    if locationManager.location != nil {
      cafeCell.configureWithCafe(cafe, colored: true, distance: locationManager.location.distanceFromLocation(cafe.location) / 1000, imageCache: imageCache)
    } else {
      cafeCell.configureWithCafe(cafe, colored: true, distance: nil, imageCache: imageCache)
    }
    return cafeCell
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    if IOS_8() {
      cafeListViewController.performSegueWithIdentifier("SegueCafeDetail", sender: datasource[indexPath.row])
    } else {
      cafeListViewController.performSegueWithIdentifier("SegueCafeDetail7", sender: datasource[indexPath.row])
    }
  }
}

extension CafeListTableViewDelegate: UIScrollViewDelegate {
  func scrollViewDidScroll(scrollView: UIScrollView) {
    delegate?.tableViewDidScroll(scrollView as! UITableView)
  }
  
  func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
    delegate?.tableViewDidStop(scrollView as! UITableView)
  }
  
  func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    if !decelerate {
      delegate?.tableViewDidStop(scrollView as! UITableView)
    }
  }
}
