//
//  SearchTableViewDelegate.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 12/10/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class SearchTableViewDelegate: NSObject {
  var searchItems: [SearchItem] = [
    SearchItem(feature: Feature(type: "wifi"), mode: false),
    SearchItem(feature: Feature(type: "attested"), mode: false),
    SearchItem(feature: Feature(type: "tasting"), mode: false),
    SearchItem(feature: Feature(type: "roasting"), mode: false),
    SearchItem(feature: Feature(type: "tnt"), mode: false),
    SearchItem(feature: Feature(type: "closetouristpoint"), mode: false),
    SearchItem(feature: Feature(type: "closemetropoint"), mode: false),
    SearchItem(feature: Feature(type: "livemusic"), mode: false),
    SearchItem(feature: Feature(type: "openair"), mode: false),
    SearchItem(feature: Feature(type: "decaffeinated"), mode: false),
    SearchItem(feature: Feature(type: "hasalternatetypes"), mode: false),
    SearchItem(feature: Feature(type: "courses"), mode: false),
    SearchItem(feature: Feature(type: "hasawards"), mode: false),
    SearchItem(feature: Feature(type: "togobuy"), mode: false),
    SearchItem(feature: Feature(type: "tobuy"), mode: false),
    
    SearchItem(feature: Feature(type: "check"), mode: false),
    SearchItem(feature: Feature(type: "accessibility"), mode: false),
    SearchItem(feature: Feature(type: "compressedair"), mode: false),
    SearchItem(feature: Feature(type: "wc"), mode: false),
    SearchItem(feature: Feature(type: "smoking"), mode: false),
    SearchItem(feature: Feature(type: "parking"), mode: false)
  ]
}

extension SearchTableViewDelegate: UITableViewDataSource, UITableViewDelegate {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return searchItems.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let searchCell = tableView.dequeueReusableCellWithIdentifier("SearchCell", forIndexPath: indexPath) as! SearchCell
    searchCell.configureWithSearchItem(searchItems[indexPath.row])
    return searchCell
  }
}
