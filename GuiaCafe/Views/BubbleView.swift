//
//  BubbleView.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 12/1/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit
import MapKit

@objc protocol BubbleViewDelegate {
  func didRemove()
  func goToCafe()
}

class BubbleView: MKAnnotationView {
  weak var delegate: BubbleViewDelegate?
  var radius: CGFloat? {
    didSet {
      if circle != nil {
        UIView.animateWithDuration(0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1.0, options: .CurveEaseInOut, animations: { () -> Void in
          var transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0)
          transform.tx = sin(self.radius!) * 100.0
          transform.ty = -cos(self.radius!) * 100.0
          self.circle!.transform = transform
          self.blackBoard!.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, sin(self.radius!) * 100.0, -cos(self.radius!) * 100.0)
          var stripeTransform = CGAffineTransformRotate(CGAffineTransformIdentity, self.radius!)
          stripeTransform.tx = sin(self.radius!) * 30.0 + 2.0
          stripeTransform.ty = -cos(self.radius!) * 30.0
          self.blackStripe!.transform = stripeTransform
        }, completion: { (finished) -> Void in
          
        })
      }
    }
  }
  var circle: UIButton?
  var blackBoard: UIView?
  var blackStripe: UIView?
  var nameLabel: UILabel?
  var addressLabel: UILabel?
  var star1ImageView: UIImageView?
  var star2ImageView: UIImageView?
  var star3ImageView: UIImageView?
  var star4ImageView: UIImageView?
  var star5ImageView: UIImageView?
  
  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    backgroundColor = UIColor.clearColor()
  }

  override init(frame: CGRect) {
    super.init(frame: frame)
    backgroundColor = UIColor.clearColor()
  }
  
  override init!(annotation: MKAnnotation!, reuseIdentifier: String!) {
    super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
    backgroundColor = UIColor.clearColor()
    
    blackStripe = UIView(frame: CGRect(x: 133.0, y: 130.0, width: 4.0, height: 35.0))
    blackStripe!.backgroundColor = UIColor(red: 37.0/255, green: 36.0/255, blue: 34.0/255, alpha: 1.0)
    addSubview(blackStripe!)
    blackStripe!.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01)
    
    circle = UIButton(frame: CGRect(x: 75, y: 85, width: 120, height: 120))
    circle!.clipsToBounds = true
    circle!.layer.cornerRadius = 60
    circle!.backgroundColor = UIColor.clearColor()
    circle!.addTarget(self, action: "buttonTapped:", forControlEvents: .TouchUpInside)
    addSubview(circle!)
    circle!.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01)
    circle!.layer.borderColor = UIColor(red: 37.0/255, green: 36.0/255, blue: 34.0/255, alpha: 1.0).CGColor
    circle!.layer.borderWidth = 4.0
    
    blackBoard = UIView(frame: CGRect(x: 70, y: 128, width: 130, height: 40))
    blackBoard!.backgroundColor = UIColor(red: 37.0/255, green: 36.0/255, blue: 34.0/255, alpha: 1.0)
    addSubview(blackBoard!)
    blackBoard!.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01)
    blackBoard!.userInteractionEnabled = false
    
    nameLabel = UILabel(frame: CGRect(x: 0, y: 4, width: blackBoard!.bounds.width, height: 10))
    nameLabel!.textColor = UIColor.whiteColor()
    nameLabel!.textAlignment = .Center
    nameLabel!.font = UIFont(name: "Clio Bold", size: 12)
    nameLabel!.minimumScaleFactor = 0.5
    blackBoard!.addSubview(nameLabel!)
    
    addressLabel = UILabel(frame: CGRect(x: 0, y: 14, width: blackBoard!.bounds.width, height: 10))
    addressLabel!.textColor = UIColor.whiteColor()
    addressLabel!.textAlignment = .Center
    addressLabel!.font = UIFont(name: "Clio Regular", size: 10)
    addressLabel!.minimumScaleFactor = 0.5
    blackBoard!.addSubview(addressLabel!)
    
    star1ImageView = UIImageView(frame: CGRect(x: 32, y: 25, width: 13, height: 12))
    blackBoard!.addSubview(star1ImageView!)
    
    star2ImageView = UIImageView(frame: CGRect(x: 45, y: 25, width: 13, height: 12))
    blackBoard!.addSubview(star2ImageView!)
    
    star3ImageView = UIImageView(frame: CGRect(x: 58, y: 25, width: 13, height: 12))
    blackBoard!.addSubview(star3ImageView!)
    
    star4ImageView = UIImageView(frame: CGRect(x: 71, y: 25, width: 13, height: 12))
    blackBoard!.addSubview(star4ImageView!)
    
    star5ImageView = UIImageView(frame: CGRect(x: 84, y: 25, width: 13, height: 12))
    blackBoard!.addSubview(star5ImageView!)
  }
  
  func configureWithCafe(cafe: Cafe) {
    if let logo = cafe.images.first {
      circle!.setImageWithString(logo, width: Int(120 * UIScreen.mainScreen().scale), height: Int(120 * UIScreen.mainScreen().scale), completion: nil)
    } else {
      circle!.setImageWithString(nil)
    }
    nameLabel?.text = cafe.name
    addressLabel?.text = cafe.address
    let rateImage = UIImage(named: "icn_rate_detail_pin")
    let rateAlmostFilledImage = UIImage(named: "icn_rate_detail_almost_filled_pin")
    let rateFilledImage = UIImage(named: "icn_rate_filled_detail_pin")
    star1ImageView!.image = cafe.rating >= 0.75 ? rateFilledImage : (cafe.rating >= 0.25 ? rateAlmostFilledImage : rateImage)
    star2ImageView!.image = cafe.rating >= 1.75 ? rateFilledImage : (cafe.rating >= 1.25 ? rateAlmostFilledImage : rateImage)
    star3ImageView!.image = cafe.rating >= 2.75 ? rateFilledImage : (cafe.rating >= 2.25 ? rateAlmostFilledImage : rateImage)
    star4ImageView!.image = cafe.rating >= 3.75 ? rateFilledImage : (cafe.rating >= 3.25 ? rateAlmostFilledImage : rateImage)
    star5ImageView!.image = cafe.rating >= 4.75 ? rateFilledImage : (cafe.rating >= 4.25 ? rateAlmostFilledImage : rateImage)
  }
  
  func setRadiusWithPoint(point: CGPoint, inView view: UIView) {
    let x = point.x / view.frame.width
    let y = point.y / view.frame.height
    if x < 0 || y < 0 || x > 1 || y > 1{
      removeView()
      return
    }
    
    let diff: CGFloat = 0.5 - y
    let absolute: CGFloat = abs(diff)
    var r: CGFloat = (135.0 - 90.0 * y) + diff / absolute * (180.0 * (1.0 - absolute)) * x
    if r < 0 {
      r = r + 360.0
    }
    radius = r / 180.0 * CGFloat(M_PI)
  }
  
  func removeView() {
    UIView.animateWithDuration(0.3, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .CurveEaseInOut, animations: { () -> Void in
      var transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01)
      transform.tx = -10
      self.transform = transform
    }) { (finished) -> Void in
      self.blackStripe!.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01)
      self.circle!.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01)
      self.blackBoard!.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01)
      if self.delegate != nil {
        self.delegate!.didRemove()
      }
    }
  }
  
  func buttonTapped(sender: UIButton) {
    delegate?.goToCafe()
  }
}
