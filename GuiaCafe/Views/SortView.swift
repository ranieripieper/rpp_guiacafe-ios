//
//  SortView.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 12/10/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

@objc protocol SortViewDelegate {
  func didChangeSortRating(on: Bool)
}

class SortView: UIView {
  @IBOutlet weak var segmentedControl: UISegmentedControl!

  weak var delegate: SortViewDelegate?
  
  @IBAction func segmentedChanged(sender: UISegmentedControl) {
    delegate?.didChangeSortRating(sender.selectedSegmentIndex == 1)
  }
  
  func setColor(color: UIColor) {
    segmentedControl.tintColor = color
  }
}
