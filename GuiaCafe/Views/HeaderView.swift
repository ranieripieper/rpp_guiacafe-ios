//
//  HeaderView.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 12/10/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class HeaderView: UIView {
  var nameLabel: UILabel!
  
  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    nameLabel = UILabel(frame: CGRect(x: 8, y: 0, width: frame.width - 16, height: frame.height))
    nameLabel.textColor = UIColor.whiteColor()
    nameLabel.font = UIFont(name: "Clio Bold", size: 16)
    addSubview(nameLabel)
  }
  
  func configureWithState(state: State) {
    nameLabel.text = state.info().0
    backgroundColor = state.info().1
  }
  
  func configureWithString(string: String) {
    nameLabel.text = string
    backgroundColor = UIColor.blackColor()
  }
}
