//
//  RateView.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 2/9/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class RateView: UIView {
  @IBOutlet weak var star1Button: UIButton!
  @IBOutlet weak var star2Button: UIButton!
  @IBOutlet weak var star3Button: UIButton!
  @IBOutlet weak var star4Button: UIButton!
  @IBOutlet weak var star5Button: UIButton!
  @IBOutlet weak var okButton: UIButton!
  
  var bgView: UIView!
  var callback: ((Bool, Int) -> ())?
  var selectedIndex = 1
  
  @IBAction func star1Tapped(sender: UIButton) {
    selectedIndex = 1
    tappedAtIndex(1)
  }
  
  @IBAction func star2Tapped(sender: UIButton) {
    selectedIndex = 2
    tappedAtIndex(2)
  }
  
  @IBAction func star3Tapped(sender: UIButton) {
    selectedIndex = 3
    tappedAtIndex(3)
  }
  
  @IBAction func star4Tapped(sender: UIButton) {
    selectedIndex = 4
    tappedAtIndex(4)
  }
  
  @IBAction func star5Tapped(sender: UIButton) {
    selectedIndex = 5
    tappedAtIndex(5)
  }
  
  @IBAction func okTapped(sender: UIButton) {
    callback?(true, selectedIndex)
  }
  
  func presentInViewController(viewController: UIViewController) {
    bgView = UIView(frame: viewController.view.bounds)
    bgView.backgroundColor = UIColor.blackColor()
    bgView.alpha = 0.0
    let tap = UITapGestureRecognizer(target: self, action: "tapped:")
    bgView.addGestureRecognizer(tap)
    frame = CGRect(x: 20.0, y: (viewController.view.bounds.height - bounds.height) / 2.0, width: viewController.view.bounds.width - 40.0, height: bounds.height)
    viewController.view.addSubview(bgView)
    viewController.view.addSubview(self)
    transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.0, 0.0)
    UIView.animateWithDuration(0.3, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: .AllowUserInteraction, animations: {
      self.bgView.alpha = 0.7
      self.transform = CGAffineTransformIdentity
    }, completion: nil)
  }
  
  func dismissFromViewController(viewController: UIViewController) {
    UIView.animateWithDuration(0.5, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: .AllowUserInteraction, animations: {
      self.bgView.alpha = 0.0
      self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.0, 0.0)
    }) { finished in
      self.bgView.removeFromSuperview()
      self.removeFromSuperview()
    }
  }
  
  func tapped(sender: UITapGestureRecognizer) {
    callback?(false, 0)
  }
  
  private func tappedAtIndex(index: Int) {
    let unfilledImage = UIImage(named: "icn_rate_detail")
    let filledImage = UIImage(named: "icn_rate_filled")
    star1Button.setImage(index >= 1 ? filledImage : unfilledImage, forState: .Normal)
    star2Button.setImage(index >= 2 ? filledImage : unfilledImage, forState: .Normal)
    star3Button.setImage(index >= 3 ? filledImage : unfilledImage, forState: .Normal)
    star4Button.setImage(index >= 4 ? filledImage : unfilledImage, forState: .Normal)
    star5Button.setImage(index >= 5 ? filledImage : unfilledImage, forState: .Normal)
  }
}
